//
//  ActionCell.swift
//  SwApp
//
//  Created by Cristian Duguet on 11/25/15.
//  Copyright © 2015 CrowdTransfer. All rights reserved.
//

import UIKit

class ActionCell: UITableViewCell {
    @IBOutlet var actionImage: UIImageView!
    @IBOutlet var title: UILabel!
    @IBOutlet var subtitle: UILabel!
}
