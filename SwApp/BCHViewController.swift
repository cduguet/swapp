//
//  BCHViewController.swift
//  SwApp
//
//  Created by Cristian Duguet on 10/4/15.
//  Copyright © 2015 CrowdTransfer. All rights reserved.
//

import UIKit

class BCHViewController: UIViewController {

    @IBOutlet weak var blur: UIVisualEffectView!
    @IBOutlet weak var button: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        blur.layer.cornerRadius = CORNER_RADIUS
        blur.layer.masksToBounds = true
        button.layer.cornerRadius = CORNER_RADIUS
        button.layer.masksToBounds = true
        // Do any additional setup after loading the view.
        self.hidesBottomBarWhenPushed = true

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
