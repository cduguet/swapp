//
//  BureauCell.swift
//  SwApp
//
//  Created by Cristian Duguet on 10/6/15.
//  Copyright © 2015 CrowdTransfer. All rights reserved.
//

import Foundation
import UIKit
import ParseUI
import Parse
import Cosmos


class BureauCell: UITableViewCell {
    // declare variables
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var starsImage: UIImageView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var mapButton: UIButton!
    @IBOutlet weak var mapLabel: UILabel!
    @IBOutlet var cellBackground: UIView!
    @IBOutlet var rating: CosmosView!
    
    func bindData(bureauOffer: PFObject) -> Bool {
        nameLabel.text = bureauOffer[BUREAU_NAME] as? String
        guard bureauOffer[BUREAU_RATING] != nil else { return false }
        rating.rating = bureauOffer[BUREAU_RATING] as! Double
        return true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        distanceLabel.text = ""
    }
    
}
