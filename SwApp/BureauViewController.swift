//
//  BureauViewController.swift
//  SwApp
//
//  Created by Cristian Duguet on 10/6/15.
//  Copyright © 2015 CrowdTransfer. All rights reserved.
//

import UIKit

class BureauViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // --------------- Navigation Bar Style --------------
        // set bar element
        let bar:UINavigationBar! = self.navigationController?.navigationBar
        let img = getImageWithColor(SWAPP_BLUE, size: CGSizeMake(1, 1))
        bar.setBackgroundImage(img, forBarMetrics: UIBarMetrics.Default)
        bar.tintColor = UIColor.whiteColor()
        bar.barStyle = UIBarStyle.Black
        bar.shadowImage = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
