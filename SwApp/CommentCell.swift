//
//  CommentCell.swift
//  SwApp
//
//  Created by Cristian Duguet on 8/16/15.
//  Copyright (c) 2015 SwApp. All rights reserved.
//

import UIKit
import ParseUI
import Parse

class CommentCell: UITableViewCell {
    
    
    @IBOutlet var commentLabel: UILabel!
    @IBOutlet var authorNameLabel: UILabel!
    @IBOutlet var authorImage: PFImageView!
    @IBOutlet weak var thumbsImage: UIImageView!
    
    func bindData(comment: PFObject)
    {
        authorImage.layer.cornerRadius = CORNER_RADIUS
        authorImage.layer.masksToBounds = true

        
        if comment["comment"] != nil {
            commentLabel.text = "\"" + (comment["comment"] as! String) + "\""
        }
        
        let opinion = comment["opinion"] as? Int
        
        if opinion  >= 3 {
            commentLabel.textColor == GLASS_GREEN
            authorNameLabel.textColor == GLASS_GREEN
            thumbsImage.image = UIImage(named: "thumbs_up")
            thumbsImage.image = thumbsImage.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            thumbsImage.tintColor = GLASS_GREEN
        } else if opinion < 3 {
            commentLabel.textColor == UIColor.redColor()
            authorNameLabel.textColor == UIColor.redColor()
            thumbsImage.image = UIImage(named: "thumbs_down")
            thumbsImage.image = thumbsImage.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            thumbsImage.tintColor = UIColor.redColor()
        }
        
        let author  = comment["author"] as? PFUser
        author?.fetchIfNeededInBackgroundWithBlock({ (object, error) -> Void in
            self.authorNameLabel.text = author?[PF_USER_FIRSTNAME] as? String
            self.authorImage.file = author?["picture"] as? PFFile
            self.authorImage.loadInBackground(nil)
        })
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}