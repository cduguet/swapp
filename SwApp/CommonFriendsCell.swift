//
//  CommonFriendsCell.swift
//  SwApp
//
//  Created by Cristian Duguet on 11/25/15.
//  Copyright © 2015 CrowdTransfer. All rights reserved.
//

import UIKit

class CommonFriendsCell: UITableViewCell {
    @IBOutlet var title: UIView!
    @IBOutlet var friendsList: UICollectionView!
}
