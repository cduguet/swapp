//
//  ConfirmationViewController.swift
//  SwApp
//
//  Created by Cristian Duguet on 7/19/15.
//  Copyright (c) 2015 CrowdTransfer. All rights reserved.
//

import UIKit

class ConfirmationViewController: UIViewController, UITextFieldDelegate {

    var user = PFUser()
    
    @IBOutlet var firstNameField: UITextField!
    @IBOutlet var lastNameField: UITextField!
    @IBOutlet var emailField: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "dismissKeyboard"))
        self.firstNameField.delegate = self
        self.lastNameField.delegate = self
        self.emailField.delegate = self
        self.loadUser()
    }
    
    @IBAction func confirmUserData(sender: AnyObject) {
        self.saveUserInformation()
    }

    
    func saveUserInformation() {
        
        let firstName = firstNameField.text!
        let lastName = lastNameField.text!
        let email = emailField.text!.lowercaseString
        
        if firstName.characters.count == 0 || lastName.characters.count == 0 {
            ProgressHUD.showError("Name must be set.")
            return
        }
        
        if email.characters.count == 0 {
            ProgressHUD.showError("Email must be set.")
            return
        }
        
        user[PF_USER_FIRSTNAME] = firstNameField.text
        user[PF_USER_LASTNAME] = lastNameField.text
        user.email = emailField.text
        
        user.saveInBackgroundWithBlock({ (succeeded: Bool, error: NSError?) -> Void in
            if error == nil {
                self.user[PF_USER_ACTIVATED] = true
                self.userLoggedIn(self.user)
            } else {
                PFUser.logOut()
                if let info = error?.userInfo {
                    ProgressHUD.showError("Login error")
                    print(info["error"] as! String)
                }
            }
        })
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.firstNameField.becomeFirstResponder()
    }
    
    func loadUser() {
        if let user = PFUser.currentUser() {
            firstNameField.text = user[PF_USER_FIRSTNAME] as? String
            lastNameField.text = user[PF_USER_LASTNAME] as? String
            emailField.text = user[PF_USER_EMAIL] as? String
        }
    }
    
//    func userLoggedIn(user: PFUser) {
//        PushNotication.parsePushUserAssign()
//        ProgressHUD.showSuccess("Welcome, \(user[PF_USER_FIRSTNAME]!)!")
//        //self.dismissViewControllerAnimated(true, completion: nil)
//        self.performSegueWithIdentifier("userDataConfirmedSegue", sender: self)
//    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    
    // MARK: - Move view when Keyboard is on
    
    func textFieldDidBeginEditing(textField: UITextField) {
        animateViewMoving(true, moveValue: 100)
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        animateViewMoving(false, moveValue: 100)
    }
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:NSTimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = CGRectOffset(self.view.frame, 0,  movement)
        UIView.commitAnimations()
    }
    
    // MARK: - Log in Function
    func userLoggedIn(user: PFUser) {
        PushNotication.parsePushUserAssign()
        dispatch_async(dispatch_get_main_queue(), {
            ProgressHUD.showSuccess("Welcome!")
        })
        self.performSegueWithIdentifier("loginSegue", sender: self)
    }
}