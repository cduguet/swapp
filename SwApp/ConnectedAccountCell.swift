//
//  ConnectedAccountCell.swift
//  SwApp
//
//  Created by Cristian Duguet on 11/25/15.
//  Copyright © 2015 CrowdTransfer. All rights reserved.
//

import UIKit

class ConnectedAccountCell: UITableViewCell {
    @IBOutlet var title: UILabel!
    @IBOutlet var accountImage: UIImageView!
}
