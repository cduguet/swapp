//
//  Constants.swift
//  SwApp
//
//  Created by Cristian Duguet on 7/19/15.
//  Copyright (c) 2015 CrowdTransfer. All rights reserved.
//

import Foundation


/* TODO: Try struct format for constants */
struct Constants {
    struct PF {
        struct Installation {
            static let ClassName = "_Installation"
        }
    }
}

//let HEXCOLOR(c) = [UIColor colorWithRed:((c>>24)&0xFF)/255.0 green:((c>>16)&0xFF)/255.0 blue:((c>>8)&0xFF)/255.0 alpha:((c)&0xFF)/255.0]

let DEFAULT_TAB							= 0

let MESSAGE_INVITE						= "Check out SwiftParseChat on GitHub: https://github.com/huyouare/SwiftParseChat"

/* Installation */
let PF_INSTALLATION_CLASS_NAME			= "_Installation"           //	Class name
let PF_INSTALLATION_OBJECTID			= "objectId"				//	String
let PF_INSTALLATION_USER				= "user"					//	Pointer to User Class

/* User */
let PF_USER_PERMISSIONS                 = ["public_profile", "email", "user_friends"] //String Array, permissions for facebook
let PF_USER_CLASS_NAME					= "User"                   //	Class name
let PF_USER_OBJECTID					= "objectId"				//	String
let PF_USER_USERNAME					= "username"				//	String
let PF_USER_PASSWORD					= "password"				//	String
let PF_USER_EMAIL						= "email"                   //	String
let PF_USER_EMAILCOPY					= "emailCopy"               //	String
let PF_USER_FIRSTNAME                   = "firstname"                //  String
let PF_USER_LASTNAME                    = "lastname"                //  String
let PF_USER_FULLNAME					= "fullname"				//	String
let PF_USER_FULLNAME_LOWER				= "fullname_lower"          //	String
let PF_USER_FACEBOOKID					= "facebookId"              //	String
let PF_USER_PICTURE						= "picture"                 //	File
let PF_USER_THUMBNAIL					= "thumbnail"               //	File
let PF_USER_MESSAGE                     = "userMessage"             //  String
let PF_USER_ACTIVATED                   = "activated"               //  Boolean Backup
let PF_USER_EMAILVERIFIED               = "emailVerified"           // Boolean to check if the user has verified his email account using Parse Email Verification Support
let PF_USER_RATING                      = "rating"                  // Number 
let PF_USER_RATERS                      = "raters"                  // Number
//let PF_USER_ISVERIFIED                  = "isVerified"              // Boolean  If the user has checked his identity Card // DEPRECATED!
let PF_USER_PASSPORTVERIFIED            = "passportVerified"        //Boolean Check the user's passprot verification
let PF_USER_TRAITYBADGE                 = "traityBadge"             // String
let PF_USER_ISTRAITY                    = "isTraity"                // Bool if the user is connected to traity
let PF_USER_CONNECTEDACCOUNTS           = "connectedAccounts"       // Array of Strings



/* Chat */
let PF_CHAT_CLASS_NAME					= "Chat"					//	Class name
let PF_CHAT_USER						= "user"					//	Pointer to User Class
let PF_CHAT_GROUPID						= "groupId"                 //	String
let PF_CHAT_TEXT						= "text"					//	String
let PF_CHAT_PICTURE						= "picture"                 //	File
let PF_CHAT_VIDEO						= "video"                   //	File
let PF_CHAT_CREATEDAT					= "createdAt"               //	Date

/* Groups */
let PF_GROUPS_CLASS_NAME				= "Groups"                  //	Class name
let PF_GROUPS_NAME                      = "name"					//	String

/* Messages*/
let PF_MESSAGES_CLASS_NAME				= "Messages"				//	Class name
let PF_MESSAGES_USER					= "user"					//	Pointer to User Class
let PF_MESSAGES_GROUPID					= "groupId"                 //	String
let PF_MESSAGES_DESCRIPTION				= "description"             //	String
let PF_MESSAGES_LASTUSER				= "lastUser"				//	Pointer to User Class
let PF_MESSAGES_LASTMESSAGE				= "lastMessage"             //	String
let PF_MESSAGES_COUNTER					= "counter"                 //	Number
let PF_MESSAGES_UPDATEDACTION			= "updatedAction"           //	Date
let PF_MESSAGES_COUNTERPART             = "counterPart"             //  PFUSer Pointer

let OFFER_STATUS_DELETED                = 0                         // Int
let OFFER_STATUS_CREATED                = 1                         // Int
let OFFER_STATUS_MODIFIED               = 2                         // Int

/* Local Notifications */
let NOTIFICATION_APP_STARTED			= "NCAppStarted"
let NOTIFICATION_USER_LOGGED_IN			= "NCUserLoggedIn"
let NOTIFICATION_USER_LOGGED_OUT		= "NCUserLoggedOut"

/* Style */
let CORNER_RADIUS                       = 10.0 as CGFloat                       // CGFloat
let SWAPP_RED                          = UIColor(red: 195/255, green: 0/255, blue: 4/255, alpha: 1)
let SWAPP_YELLOW                       = UIColor(red: 255/255, green: 189/255, blue: 30/255, alpha: 1)
let SWAPP_BLUE                          = UIColor(red: 25/255, green: 80/255, blue: 145/255, alpha: 1)
let GLASS_GREEN                         = UIColor(red: 124/255, green: 178/255, blue: 40/255, alpha: 1)
let GLASS_BLUE                          = UIColor(red: 80/255, green: 179/255, blue: 226/255, alpha: 1)

let FACEBOOK_BLUE                       = UIColor(red: 59/255, green: 89/255, blue: 152/255, alpha: 1)



/* Currencies */
let CURRENCIES_GLOBAL                   = ["USD", "EUR", "ARS", "AUD", "BRL", "CAD", "CLP", "CNY", "COP", "GBP", "INR"] // [String]
let MAX_DISTANCE_MATCH                  = 15000.0 as Double // Double: Max distance within results are going to show. (KM)

let BLINK_LICENSE = "3YHOFR5O-KMXH7HQO-ZNPP2UJC-HSITZEJ4-SE6JCPER-HSITZEJ4-SE6JCPER-GS4HRPES";


//IDENTIFICATION DATA

let ID_FIRSTNAME                        = "IDfirstName"
let ID_LASTNAME                         = "IDreallastName"
let ID_ISSUER                           = "IDissuer"
let ID_NUMBER                           = "IDnumber"
let ID_EXPIRATION                       = "IDexpiration"
let ID_BIRTH                            = "IDbirth"
let ID_NATIONALITY                      = "IDnationality"
let ID_SEX                              = "IDsex"
let ID_SERIAL                           = "IDserialNumber"
let ID_CODE                             = "IDcode"
let ID_VALID                            = "IDvalid"



// Bureau de Change Offer
let BUREAU_NAME                         = "name"
let BUREAU_RATING                       = "reputation"
let BUREAU_LOCATION                     = "location"
let BUREAU_EXPIRE                       = "expires"
let BUREAU_PHONE                        = "phoneNumber"
let BUREAU_WEBSITE                      = "webSite"
let BUREAU_WEEK_OPENS                   = "weekOpen"
let BUREAU_WEEK_CLOSES                  = "weekCloses"
let BUREAU_SAT_OPENS                    = "satOpens"
let BUREAU_SAT_CLOSES                   = "satCloses"
let BUREAU_SUN_OPENS                    = "sunOpens"
let BUREAU_SUN_CLOSES                   = "sunCloses"


let MAX_BUREAU_OFFERS                   = 2 




