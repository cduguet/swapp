//
//  DropDownField.swift
//  SwApp
//
//  Created by Cristian Duguet on 9/27/15.
//  Copyright © 2015 CrowdTransfer. All rights reserved.
//

import Foundation

class DropDownTextField: UITextField {
    let inset: CGFloat = 15
    
    //    override func canBecomeFirstResponder() -> Bool {
    //        return false
    //    }
    
    override func canPerformAction(action: Selector, withSender sender: AnyObject?) -> Bool {
        //if action == "paste:" {return false}
        //return super.canPerformAction(action, withSender: sender)
        return false
    }
    
    // placeholder position
    override func textRectForBounds(bounds: CGRect) -> CGRect {
        return CGRectInset(bounds , inset , 0)
    }
    
    // text position
    override func editingRectForBounds(bounds: CGRect) -> CGRect {
        return CGRectInset(bounds , inset, 0)
    }
    
    override func caretRectForPosition(position: UITextPosition) -> CGRect {
        return CGRectZero;
    }
}