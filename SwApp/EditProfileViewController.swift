//
//  EditProfileViewController.swift
//  SwApp
//
//  Created by Cristian Duguet on 8/10/15.
//  Copyright (c) 2015 CrowdTransfer. All rights reserved.
//

import UIKit
import ParseUI
import Social

class EditProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate {

    @IBOutlet var userImageView: PFImageView!
    @IBOutlet var firstNameField: FormTextField!
    @IBOutlet var lastNameField: FormTextField!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var userMessageTextView: UITextView!
   
    @IBOutlet var saveButton: UIButton!
    @IBOutlet var imageButton: UIButton!
    
    let PLACEHOLDER_TEXT = "Tell them a little bit about you, and why do you want to exchange."
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // --------------- Navigation Bar Style --------------
        let bar:UINavigationBar! = self.navigationController?.navigationBar
        let img = getImageWithColor(SWAPP_BLUE, size: CGSizeMake(1, 1))
        bar.setBackgroundImage(img, forBarMetrics: UIBarMetrics.Default)
        bar.tintColor = UIColor.whiteColor()
        bar.barStyle = UIBarStyle.Black
        bar.shadowImage = nil
        
        
        // button Styles
        saveButton.layer.cornerRadius = 5
        saveButton.layer.borderWidth = 1
        saveButton.layer.borderColor = UIColor.blackColor().CGColor

        
        
        // TextView Placeholder
        self.view.addSubview(userMessageTextView)
        applyPlaceholderStyle(userMessageTextView!, placeholderText: PLACEHOLDER_TEXT)
        userMessageTextView?.delegate = self
        
        //Styles
        userMessageTextView.textContainerInset = UIEdgeInsetsMake(8,5,8,5);
        firstNameField.layoutMargins = UIEdgeInsetsMake(5,8,5,8);
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "dismissKeyboard"))
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        let currentUser = PFUser.currentUser()
        if currentUser != nil {
            if currentUser![PF_USER_ACTIVATED] as? Bool == true {
                loadUser()
            } else {
                Utilities.logout(self)
            }
        } else {
            Utilities.logout(self)
        }
        
        // User Image Styling
        userImageView.layer.cornerRadius = CORNER_RADIUS
        userImageView.layer.masksToBounds = true;
    }
    
    func loadUser() {
            if let user = PFUser.currentUser() {
            userImageView.file = user[PF_USER_PICTURE] as? PFFile
            userImageView.loadInBackground { (image: UIImage?, error: NSError?) -> Void in
                if error != nil {
                    print(error)
                }
            }
            //println(user)
            firstNameField.text = user[PF_USER_FIRSTNAME] as? String
            lastNameField.text = user[PF_USER_LASTNAME] as? String
            emailLabel.text = user[PF_USER_EMAIL] as? String
            if let message: String = user[PF_USER_MESSAGE] as? String {
                if !message.isEmpty {
                    applyNonPlaceholderStyle(userMessageTextView)
                    userMessageTextView.text = message
                }
            }
        } else {
            print("The user is not showing");
        }
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    @IBAction func photoButtonPressed(sender: AnyObject) {
        Camera.shouldStartPhotoLibrary(self, canEdit: true)
    }
    
    @IBAction func saveButtonPressed(sender: AnyObject) {
        self.dismissKeyboard()
        self.saveUser()
    }
    
    func saveUser() {
        let firstName = firstNameField.text!
        let lastName = lastNameField.text!
        if firstName.characters.count > 0 && lastName.characters.count > 0 {
            if let user = PFUser.currentUser() {
                user[PF_USER_FIRSTNAME] = firstName
                user[PF_USER_LASTNAME] = lastName
                user[PF_USER_FULLNAME] = firstName + " " + lastName
                user[PF_USER_FULLNAME_LOWER] = user[PF_USER_FULLNAME]!.lowercaseString
                user[PF_USER_MESSAGE] = userMessageTextView.text
                user.saveInBackgroundWithBlock({ (succeeded: Bool, error: NSError?) -> Void in
                    if error == nil {
                        ProgressHUD.showSuccess("Saved")
                    } else {
                        ProgressHUD.showError("Network error")
                    }
                })
            }
        } else {
            ProgressHUD.showError("Name field must not be empty")
        }
    }

    
    
    // MARK: - UIImagePickerControllerDelegate
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        var image = info[UIImagePickerControllerEditedImage] as! UIImage
        if image.size.width > 280 {
            image = Images.resizeImage(image, width: 280, height: 280)!
        }
        
        let pictureFile = PFFile(name: "picture.jpg", data: UIImageJPEGRepresentation(image, 0.6)!)
        pictureFile.saveInBackgroundWithBlock { (succeeded: Bool, error: NSError?) -> Void in
            if error != nil {
                ProgressHUD.showError("Network error")
            }
        }
        
        userImageView.image = image
        
        if image.size.width > 60 {
            image = Images.resizeImage(image, width: 60, height: 60)!
        }
        
        let thumbnailFile = PFFile(name: "thumbnail.jpg", data: UIImageJPEGRepresentation(image, 0.6)!)
        thumbnailFile.saveInBackgroundWithBlock { (succeeded: Bool, error: NSError?) -> Void in
            if error != nil {
                ProgressHUD.showError("Network error")
            }
        }
        
        if let user = PFUser.currentUser() {
            user[PF_USER_PICTURE] = pictureFile
            user[PF_USER_THUMBNAIL] = thumbnailFile
            user.saveInBackgroundWithBlock { (succeeded: Bool, error: NSError?) -> Void in
                if error != nil {
                    ProgressHUD.showError("Network error")
                }
            }
        }
        picker.dismissViewControllerAnimated(true, completion: nil)
    }

    
    
    // Mark: - Create TextView Placeholder
    
    func applyPlaceholderStyle(aTextview: UITextView, placeholderText: String)
    {
        // make it look (initially) like a placeholder
        aTextview.textColor = UIColor.lightGrayColor()
        aTextview.text = placeholderText
    }
    
    func applyNonPlaceholderStyle(aTextview: UITextView)
    {
        // make it look like normal text instead of a placeholder
        aTextview.textColor = UIColor.darkTextColor()
        aTextview.alpha = 1.0
    }
    func textViewShouldBeginEditing(aTextView: UITextView) -> Bool
    {
        if aTextView == userMessageTextView && aTextView.text == PLACEHOLDER_TEXT
        {
            // move cursor to start
            moveCursorToStart(aTextView)
        }
        return true
    }
    func moveCursorToStart(aTextView: UITextView)
    {
        dispatch_async(dispatch_get_main_queue(), {
            aTextView.selectedRange = NSMakeRange(0, 0);
        })
    }
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        // remove the placeholder text when they start typing
        // first, see if the field is empty
        // if it's not empty, then the text should be black and not italic
        // BUT, we also need to remove the placeholder text if that's the only text
        // if it is empty, then the text should be the placeholder
        let newLength = "textView.text".utf16.count + text.utf16.count - range.length
        if newLength > 0 // have text, so don't show the placeholder
        {
            // check if the only text is the placeholder and remove it if needed
            // unless they've hit the delete button with the placeholder displayed
            if textView == userMessageTextView && textView.text == PLACEHOLDER_TEXT
            {
                if text.utf16.count == 0 // they hit the back button
                {
                    return false // ignore it
                }
                applyNonPlaceholderStyle(textView)
                textView.text = ""
            }
            return true
        }
        else  // no text, so show the placeholder
        {
            applyPlaceholderStyle(textView, placeholderText: PLACEHOLDER_TEXT)
            moveCursorToStart(textView)
            return false
        }
    }
    
    // Other
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
