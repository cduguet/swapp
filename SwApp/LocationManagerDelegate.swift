//
//  LocationManagerDelegate.swift
//  SwApp
//
//  Created by Cristian Duguet on 10/1/15.
//  Copyright © 2015 CrowdTransfer. All rights reserved.
//
// This is a singleton made to manage the location querys


import Foundation
import CoreLocation

class LocationManagerDelegate : NSObject, CLLocationManagerDelegate {
    static let sharedInstance = LocationManagerDelegate()
    
    var locationManager: CLLocationManager?
    var currentLocation: CLLocation?
    
    private override init () {
        super.init()
        self.locationManager = CLLocationManager()
        self.locationManager?.requestWhenInUseAuthorization()
        self.locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager?.distanceFilter  = 200
        self.locationManager?.delegate = self
    }
    
    func startUpdatingLocation() {
        print("Starting Location Updates")
        self.locationManager?.startUpdatingLocation()
    }
    
    func stopUpdatingLocation() {
        print("Stop Location Updates")
        self.locationManager?.stopUpdatingLocation()
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: AnyObject? = (locations as NSArray).lastObject
        self.currentLocation = location as? CLLocation
        
//        let userLocation:CLLocation = locations[0]
//        // USER LATITUDE AND LONGITUDE
//        let latitude = userLocation.coordinate.latitude
//        let longitude = userLocation.coordinate.longitude
        
        NSNotificationCenter.defaultCenter().postNotificationName("updatedLocation", object: self, userInfo: ["location" : location!])

        //GEOCODER
        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error)->Void in
            if (error != nil) {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }
            if placemarks!.count > 0 {
                let pm = placemarks![0] as CLPlacemark
                NSNotificationCenter.defaultCenter().postNotificationName("updatedLocationName", object: self, userInfo: ["locationName" : self.displayLocationInfo(pm)])
            } else {
                print("Problem with the data received from geocoder")
            }
        })
        
    }
    
    //************ displayLocationInfo ****************
    // update placemark based on Location
    func displayLocationInfo(placemark: CLPlacemark?) -> String {
        if let containsPlacemark = placemark {
            //stop updating location to save battery life
            locationManager?.stopUpdatingLocation()
            let locality = containsPlacemark.locality ?? ""
            //let administrativeArea = containsPlacemark.administrativeArea ?? ""
            let country = containsPlacemark.country ?? ""
            return "\(locality), \(country)"
            //locationButton.setTitle("\(locality), \(country)", forState: UIControlState.Normal)
        } else { return "" }
    }
    
    
    //************ locationManager: didFailWithError ****************
    // If there is a problem reciving the Location Updates
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Error while updating location " + error.localizedDescription)
    }
    
    @IBAction func locationButtonPressed(sender: AnyObject) {
        
    }
    
//    func updateLocation(currentLocation: CLLocation) {
//        let lat = currentLocation.coordinate.latitude
//        let lon = currentLocation.coordinate.longitude
//    }
    
}