//
//  LogInViewController.swift
//  
//
//  Created by Cristian Duguet on 7/19/15.
//
//

import UIKit

class LogInViewController: UIViewController, UINavigationControllerDelegate, UITextFieldDelegate {

    @IBOutlet var usernameField: UITextField!
    @IBOutlet var passwordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "dismissKeyboard"))
        self.usernameField.delegate = self
        self.passwordField.delegate = self
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.usernameField.becomeFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == self.usernameField {
            self.passwordField.becomeFirstResponder()
        } else if textField == self.passwordField {
            self.login()
        }
        return true
    }

    
    @IBAction func loginButtonPressed(sender: AnyObject) {
        self.login();
    }
    
    
    @IBAction func recoverPassword(sender: AnyObject) {
        let username = usernameField.text!.lowercaseString
        if username.characters.count == 0 {
            ProgressHUD.showError("Please enter your email address.")
            return
        }
        PFUser.requestPasswordResetForEmailInBackground(username, block: { (success, error) -> Void in
            if error == nil {
                ProgressHUD.showSuccess("Email sent to" + username)
            } else {
                if let err: String = error!.userInfo["NSLocalizedDescription"] as? String {
                    ProgressHUD.showError(err + " " + username)
                } else { ProgressHUD.showError("An error ocurred") }
            }
        })
        PFUser.requestPasswordResetForEmailInBackground(username)
    }
    
    // MARK: - Move view when Keyboard is on
    
    func textFieldDidBeginEditing(textField: UITextField) {
        animateViewMoving(true, moveValue: 100)
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        animateViewMoving(false, moveValue: 100)
    }
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:NSTimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = CGRectOffset(self.view.frame, 0,  movement)
        UIView.commitAnimations()
    }
    
    
    // MARK: - Log In Function
    
    func login() {
        let username = usernameField.text!.lowercaseString
        let password = passwordField.text! as String
        
        if username.characters.count == 0 {
            ProgressHUD.showError("Email field is empty.")
            return
        } else {
            ProgressHUD.showError("Password field is empty.")
        }
        
        ProgressHUD.show("Signing in...", interaction: true)
        
        PFUser.logInWithUsernameInBackground(username, password: password) { (user: PFUser?, error: NSError?) -> Void in
            if user != nil {
                
                //Check if the user  has verified their email address
                // update: incorporated resendconfirmation for users before email Confirmation activated
                let userEmailVerified = user!.objectForKey(PF_USER_EMAILVERIFIED)?.boolValue ?? false
                if userEmailVerified {
                    PushNotication.parsePushUserAssign()
                    ProgressHUD.showSuccess("Welcome back!")
                    //self.dismissViewControllerAnimated(true, completion: nil)
                    //self.dismissViewControllerAnimated(true, completion: nil)
                    self.performSegueWithIdentifier("loginSegue", sender: self)
                } else {
                    let alertController = UIAlertController(title: "Email address not verified",
                        message: "We have sent you an email to confirm your address. Please click on the link it contains.",
                        preferredStyle: UIAlertControllerStyle.Alert
                    )
                    alertController.addAction(UIAlertAction(title: "OK",
                        style: UIAlertActionStyle.Default,
                        handler: { alertController in
                            PFUser.logOut()
                            self.navigationController?.popViewControllerAnimated(true)
                            
                    }))
                    alertController.addAction(UIAlertAction(title: NSLocalizedString("Resend Confirmation", comment: "To Resend Confirmation"), style: .Default, handler: { action in
                        let tempEmailAddress = user!.email
                        user!.email = "no-reply@parseapps.com"
                        let _ = try? user!.save()
                        user!.email = tempEmailAddress
                        let _ = try? user!.save()
                        let alertController = UIAlertController(title: "Email address verification",
                            message: "We have sent you an email that contains a link - you must click this link before you can continue.",
                            preferredStyle: UIAlertControllerStyle.Alert
                        )
                        alertController.addAction(UIAlertAction(title: "OK",
                            style: UIAlertActionStyle.Default,
                            handler: { alertController in
                                PFUser.logOut()
                                self.navigationController?.popViewControllerAnimated(true)})
                        )
                        // Display alert
                        self.presentViewController(alertController, animated: true, completion: nil)
                        
                    }))
                    // Display alert
                    self.presentViewController(alertController, animated: true, completion: nil)
                }
            } else {
                if let info = error?.userInfo {
                    ProgressHUD.showError(info["error"] as! String)
                }
            }
        }
    }
    
    func checkEmailVerification(user: PFUser!) -> Bool {
        if !(user.objectForKey(PF_USER_EMAILVERIFIED)!.boolValue) {
            let i = try? user.fetch()
            if i == nil { return false }
            if !(user.objectForKey(PF_USER_EMAILVERIFIED)!.boolValue) {
                return false
            }
            else { return true }
        } else {return true }
    }
}
