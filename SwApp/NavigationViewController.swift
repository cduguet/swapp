//
//  NavigationViewController.swift
//  SwApp
//
//  Created by Cristian Duguet on 10/26/15.
//  Copyright © 2015 CrowdTransfer. All rights reserved.
//

import UIKit
import MediaPlayer

class NavigationViewController: UINavigationController {

    var moviePlayer: MPMoviePlayerController!

    override func viewDidLoad() {
        super.viewDidLoad()

        let videoURL = NSBundle.mainBundle().URLForResource("backgroundVideo", withExtension: "mp4")
        
        self.moviePlayer = MPMoviePlayerController(contentURL: videoURL)
        
        self.moviePlayer.controlStyle = MPMovieControlStyle.None
        self.moviePlayer.scalingMode = MPMovieScalingMode.AspectFill
        
        self.moviePlayer.view.frame = self.view.frame
        self.view .insertSubview(self.moviePlayer.view, atIndex: 0)
        
        self.moviePlayer.play()
        
        // Loop video.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "loopVideo", name: MPMoviePlayerPlaybackDidFinishNotification, object: self.moviePlayer)
        
        

    }
    func loopVideo() {
        self.moviePlayer.play()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
