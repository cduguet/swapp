//
//  OffersCell.swift
//  SwApp
//
//  Created by Cristian Duguet on 7/19/15.
//  Copyright (c) 2015 CrowdTransfer. All rights reserved.
//


import UIKit
import ParseUI
import Parse
import Cosmos

class OfferCell: UITableViewCell {
    
    // declare variables
    @IBOutlet var userImage: PFImageView!
    @IBOutlet var userName: UILabel!
    @IBOutlet var amountFrom: UILabel!
    @IBOutlet var amountTo: UILabel!
    @IBOutlet var currencyFrom: UILabel!
    @IBOutlet var currencyTo: UILabel!
    @IBOutlet var flagFrom: UIImageView!
    @IBOutlet var flagTo: UIImageView!
    @IBOutlet var expiring: UILabel!
    @IBOutlet var hasLabel: UILabel!
    @IBOutlet var wantsLabel: UILabel!
    @IBOutlet var userButton: UIButton!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var cellBackground: UIView!
    @IBOutlet var badge: UIImageView!
    
    
    @IBOutlet var rating: CosmosView!
    
    
    func bindData(offer: PFObject) -> Bool {
        //FIXME: Check for null elements of class Offer
        userImage.layer.cornerRadius = CORNER_RADIUS
        userImage.layer.masksToBounds = true
        let offerUser  = offer["user"] as? PFUser
        offerUser?.fetchIfNeededInBackgroundWithBlock({ (object, error) -> Void in
            self.userImage.file = offerUser!["picture"] as? PFFile
            self.userName.text = offerUser?[PF_USER_FIRSTNAME] as? String

            if let h = offerUser![PF_USER_TRAITYBADGE] as? traityBadgeType.RawValue {
                let x = offerUser![PF_USER_ISTRAITY] as? Bool ?? false
                self.badge.hidden = !x
                self.badge.image = UIImage(named: "traity-big-level-\(h)")
            }
            
            self.rating.rating = offerUser![PF_USER_RATING] as? Double ?? 0.0
        })
        userImage.loadInBackground(nil)
        currencyFrom.text = offer["currencyFrom"] as? String
        currencyTo.text = offer["currencyTo"] as? String
        amountFrom.text = Utilities.currencyFormatter(offer["amountFrom"] as! CGFloat,currency: currencyFrom.text!)
        amountTo.text = Utilities.currencyFormatter(offer["amountTo"] as! CGFloat,currency: currencyTo.text!)
        let seconds = NSDate().timeIntervalSinceDate(offer["expiration"] as! NSDate)
        expiring.text = Utilities.timeToExpire(seconds * -1)
        flagFrom.image = UIImage(named: String((offer["currencyFrom"]!.lowercaseString as String ) + "Flag.png"))
        flagTo.image = UIImage(named: String((offer["currencyTo"]!.lowercaseString as String ) + "Flag.png"))
        return true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        badge.hidden = true
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        userImage.image = UIImage(named: "avatar-placeholder")
        distanceLabel.text = ""
        badge.hidden = true
    }
    
}
