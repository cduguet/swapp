////
////  OffersPFQueryTableViewController.swift
////  SwApp
////
////  Created by Cristian Duguet on 10/1/15.
////  Copyright © 2015 CrowdTransfer. All rights reserved.
////
//import Foundation
//import Parse
//import ParseUI
//
//class OffersPFQueryTableViewController: PFQueryTableViewController {
//    
//    // Empty View
//    var emptyView: UIView!
//    
//    //Offers List
//    var offers = [PFObject]()
//    
//    //MARK: -Search Parameters from SwapViewController -------------
//    var searchCurrencyFrom =  ""
//    var searchCurrencyTo = ""
//    var searchAmountFrom: CGFloat?
//    var searchAmountTo: CGFloat?
//    var searchLocation: PFGeoPoint?
//    
//    //MARK: -Swapper user from  ProfileViewController ---------------
//    var swapper: PFUser?
//    
//    
//    //MARK: -from PFQueryTableViewController ---------------------------
//    override init(style: UITableViewStyle, className: String?) {
//        super.init(style: style, className: className)
//        parseClassName = "Todo"
//        pullToRefreshEnabled = true
//        paginationEnabled = true
//        objectsPerPage = 25
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        parseClassName = "Todo"
//        pullToRefreshEnabled = true
//        paginationEnabled = true
//        objectsPerPage = 25
//    }
//    
//    
//    //MARK: -ViewDidLoad --------------------------------------------------
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        // Style
//        let bar:UINavigationBar! = self.navigationController?.navigationBar
//        let img = getImageWithColor(SWAPP_BLUE, size: CGSizeMake(1, 1))
//        bar.setBackgroundImage(img, forBarMetrics: UIBarMetrics.Default)
//        bar.tintColor = UIColor.blackColor()
//        bar.shadowImage = nil
//        
//        
//        //TODO: Add Refresher
//        
//        self.emptyView?.hidden = true
//        
//        //updateFeed() { }
//        
//        LocationManagerDelegate.sharedInstance.startUpdatingLocation()
//    }
//    
//    func updateEmptyView() {
//        self.emptyView?.hidden = (self.offers.count != 0)
//    }
//    
//    func refresh() {
//        //updateFeed() { }
//    }
//    
//    //MARK: -Update Users from Server -------------------------------------------------
//    
//    //    func updateFeed(completionHandler: (() -> Void)) {
//    //        //TODO: Add diferentiation depending on the original VewController (parent)
//    //        // fromUser will be used when showed from ProfileViewController
//    //        downloadOffers(searchCurrencyFrom, currencyTo: searchCurrencyTo, amountFrom: searchAmountFrom, fromUser: swapper,location: searchLocation) { (result: [PFObject]?) -> Void in
//    //            self.offers.removeAll(keepCapacity: false)
//    //            self.offers += result!
//    //            self.tableView.reloadData()
//    //            self.updateEmptyView()
//    //            completionHandler()
//    //        }
//    //    }
//    
//    //MARK: -Parse PFQueryTable ------------------------------------------------------
//    
//    override func queryForTable() -> PFQuery {
//        let query = PFQuery(className: self.parseClassName!)
//        
//        let fromUser = swapper
//        let currencyFrom = searchCurrencyFrom ?? ""
//        let currencyTo = searchCurrencyTo ?? ""
//        let amountFrom = searchAmountFrom
//        let location = searchLocation
//        // Tolerance of price difference with search
//        let factor = 2.0 as CGFloat
//        
//        if self.objects!.count == 0 {
//            query.cachePolicy = .CacheThenNetwork
//        }
//        
//        let offersQuery = PFQuery(className: "Offers")
//        offersQuery.whereKey("status", notEqualTo: OFFER_STATUS_DELETED)
//        offersQuery.whereKey("expiration", greaterThanOrEqualTo: NSDate())
//        
//        if fromUser != nil {
//            offersQuery.whereKey("user", equalTo: fromUser!)
//        } else {
//            offersQuery.whereKey("user", notEqualTo: PFUser.currentUser()!)
//            if location != nil { offersQuery.whereKey("location", nearGeoPoint: location!, withinKilometers: 100) }
//        }
//        offersQuery.includeKey("user")
//        
//        
//        if (currencyFrom != "" && currencyTo != "" && amountFrom != nil) {
//            offersQuery.whereKey("currencyFrom", equalTo: currencyFrom)
//            offersQuery.whereKey("currencyTo", equalTo: currencyTo)
//            offersQuery.whereKey("amountFrom", greaterThanOrEqualTo: amountFrom!/factor)
//            offersQuery.whereKey("amountFrom", lessThanOrEqualTo: amountFrom!*factor)
//        }
//        return offersQuery
//    }
//    
//    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath, object: PFObject?) -> PFTableViewCell? {
//        let cell:OfferCell = self.tableView.dequeueReusableCellWithIdentifier("offerCell") as! OfferCell
//        if (offers.count > 0) {
//            cell.hasLabel.layer.borderWidth = 1
//            cell.hasLabel.layer.borderColor = UIColor.grayColor().CGColor
//            cell.wantsLabel.layer.borderWidth = 1
//            cell.wantsLabel.layer.borderColor = UIColor.grayColor().CGColor
//            
//            cell.bindData(object!)
//            cell.userButton.tag = indexPath.row
//            cell.userButton.addTarget(self, action: "gotoProfile:", forControlEvents: UIControlEvents.TouchUpInside)
//            //Distance Calculation: direct Implementation
//            let currentLocationInCL = LocationManagerDelegate.sharedInstance.currentLocation
//            let currentLocation : PFGeoPoint? = PFGeoPoint(location: currentLocationInCL)
//            let postLocation = object!["location"] as? PFGeoPoint
//            if currentLocation != nil && postLocation != nil {
//                cell.distanceLabel.text = "\(postLocation!.distanceInKilometersTo(currentLocation)) kms away"
//            }
//        }
//        return cell
//    }
//    
//    
//    //MARK: -Table Stuff -------------------------------------------------------------
//    
//    //    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//    //        return 1
//    //    }
//    
//    //    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//    //        return offers.count
//    //    }
//    //
//    //Changed for PArse analog
//    //    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//    //
//    //        let cell:OfferCell = self.tableView.dequeueReusableCellWithIdentifier("offerCell") as! OfferCell
//    //        if (offers.count > 0) {
//    //
//    //            cell.hasLabel.layer.borderWidth = 1
//    //            cell.hasLabel.layer.borderColor = UIColor.grayColor().CGColor
//    //            cell.wantsLabel.layer.borderWidth = 1
//    //            cell.wantsLabel.layer.borderColor = UIColor.grayColor().CGColor
//    //
//    //            cell.bindData(self.offers[indexPath.row])
//    //            cell.userButton.tag = indexPath.row
//    //            cell.userButton.addTarget(self, action: "gotoProfile:", forControlEvents: UIControlEvents.TouchUpInside)
//    //
//    //
//    //            //Distance Calculation: direct Implementation
//    //            let currentLocationInCL = LocationManagerDelegate.sharedInstance.currentLocation
//    //            let currentLocation : PFGeoPoint? = PFGeoPoint(location: currentLocationInCL)
//    //            let postLocation = self.offers[indexPath.row]["location"] as? PFGeoPoint
//    //            if currentLocation != nil && postLocation != nil {
//    //                cell.distanceLabel.text = "\(postLocation!.distanceInKilometersTo(currentLocation)) kms away"
//    //            }
//    //        }
//    //        return cell
//    //    }
//    
//    // Selection
//    
//    
//    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        tableView.deselectRowAtIndexPath(indexPath, animated: true)
//        let o = objects as! [PFObject]
//        let offer = o[indexPath.row] as PFObject
//        guard offer["user"] != nil else {
//            ProgressHUD.showError("User deleted")
//            return()
//        }
//        
//        let user2 : PFUser = offer["user"] as! PFUser
//        let user1 = PFUser.currentUser()
//        let groupId = Messages.startPrivateChat(user1!, user2: user2)
//        //print(groupId)
//        self.openChat(groupId)
//    }
//    
//    // Edition
//    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
//        let o = objects as! [PFObject]
//        if let u = o[indexPath.row]["user"] as? PFObject {
//            if u == PFUser.currentUser() {
//                return true
//            } else {
//                return false
//            }
//        } else { return false }
//    }
//    
//    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
//        if editingStyle == .Delete {
//            let o = objects as! [PFObject]
//            o[indexPath.row]["status"] = OFFER_STATUS_DELETED
//            o[indexPath.row].saveInBackgroundWithBlock({ (success, error) -> Void in
//                if error == nil {
//                    print("Succesfully deleted offer")
//                    self.offers.removeAtIndex(indexPath.row)
//                    tableView.beginUpdates()
//                    // Delete the row from the data source
//                    self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
//                    self.tableView.endUpdates()
//                } else { print("error deleting") }
//            })
//        }
//    }
//    
//    //MARK: -System --------------------------------------------------------------------
//    
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//    
//    override func viewDidAppear(animated: Bool) {
//        super.viewDidAppear(animated)
//        let currentUser = PFUser.currentUser()
//        if currentUser != nil {
//            if currentUser![PF_USER_ACTIVATED] as? Bool == true {
//            } else {
//                Utilities.logout(self)
//            }
//        } else {
//            Utilities.logout(self)
//        }
//    }
//    
//    
//    // MARK: - Navigation --------------------------------------------------------------
//    func gotoProfile(sender: UIButton) {
//        let index = sender.tag
//        //print("go to Profile of user in cell \(index)")
//        performSegueWithIdentifier("gotoProfileSegue", sender: index)
//    }
//    
//    func openChat(groupId: String) {
//        self.performSegueWithIdentifier("exchangeChatSegue", sender: groupId)
//    }
//    
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        if segue.identifier == "exchangeChatSegue" {
//            let chatVC = segue.destinationViewController as! ChatViewController
//            chatVC.hidesBottomBarWhenPushed = true
//            let groupId = sender as! String
//            chatVC.groupId = groupId
//        }
//        if segue.identifier == "gotoProfileSegue" {
//            let profileVC = segue.destinationViewController as! ProfileViewController
//            let index = sender as! Int
//            let user = offers[index]["user"] as! PFUser
//            profileVC.user2 = user
//        }
//    }
//}