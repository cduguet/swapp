//
//  OffersTableViewController.swift
//  SwApp
//
//  Created by Cristian Duguet on 10/1/15.
//  Copyright © 2015 CrowdTransfer. All rights reserved.
//

import Foundation
import Social


class OffersTableViewController: UITableViewController {
    
    // Empty View
    @IBOutlet var emptyView: UIView!
    
    
    //Offers List
    var offers = [PFObject]()
    var bureauOffers = [PFObject]()
    
    //MARK: -Search Parameters from SwapViewController -------------
    var searchCurrencyFrom =  ""
    var searchCurrencyTo = ""
    var searchAmountFrom: NSNumber?
    var searchAmountTo: NSNumber?
    var searchLocation: PFGeoPoint?
    
    //MARK: -Swapper user from  ProfileViewController ---------------
    var swapper: PFUser?
    var selectedCounterPart: PFUser?
    
    
    //MARK: -ViewDidLoad --------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Style
        let bar:UINavigationBar! = self.navigationController?.navigationBar
        let img = getImageWithColor(SWAPP_BLUE, size: CGSizeMake(1, 1))
        bar.setBackgroundImage(img, forBarMetrics: UIBarMetrics.Default)
        bar.tintColor = UIColor.whiteColor()
        bar.barStyle = UIBarStyle.Black

        bar.shadowImage = nil
        
        //TODO: Add Refresher
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: "refresh", forControlEvents: UIControlEvents.ValueChanged)
        self.tableView?.addSubview(self.refreshControl!)
        
        self.emptyView?.hidden = true

        ProgressHUD.show("Loading...", interaction: false)
        updateFeed() { ProgressHUD.dismiss() }
        
        LocationManagerDelegate.sharedInstance.startUpdatingLocation()
    }
    
    func updateEmptyView() {
        self.emptyView?.hidden = (self.offers.count != 0)
    }
    
    func refresh() {
        updateFeed() { self.refreshControl!.endRefreshing() }
    }

    //MARK: -Update Users from Server -------------------------------------------------
    
    func updateFeed(completionHandler: (() -> Void)) {
        //TODO: Add diferentiation depending on the original VewController (parent)
        // fromUser will be used when showed from ProfileViewController
        downloadOffers(searchCurrencyFrom, currencyTo: searchCurrencyTo, amountFrom: searchAmountFrom, fromUser: swapper,location: searchLocation) { (result: [PFObject]?) -> Void in
            self.offers.removeAll(keepCapacity: false)
            self.offers += result!
            self.tableView.reloadData()
            self.updateEmptyView()
            completionHandler()
        }
        
        downloadBureauOffers(searchCurrencyFrom, currencyTo: searchCurrencyTo, amountFrom: searchAmountFrom, fromUser: swapper,location: searchLocation) { (result: [PFObject]?) -> Void in
            self.bureauOffers.removeAll(keepCapacity: false)
            self.bureauOffers += result!
            self.tableView.reloadData()
            self.updateEmptyView()
            completionHandler()
        }
    }
    
    

    //MARK: -Table Stuff -------------------------------------------------------------
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            //TODO: Add Exchange Houses
            return bureauOffers.count
        case 1:
            return offers.count
        default:
            return 0
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // TODO: Add Exchange Houses
        
        switch indexPath.section {
        case 0:
            let cell:BureauCell = self.tableView.dequeueReusableCellWithIdentifier("bureauCell") as! BureauCell
            
            if (bureauOffers.count > 0) {
                //Style
                cell.cellBackground.layer.cornerRadius = CORNER_RADIUS
                cell.cellBackground.layer.masksToBounds = true
                cell.bindData(self.bureauOffers[indexPath.row])
                cell.phoneButton.tag = indexPath.row
                cell.phoneButton.addTarget(self, action: "callNumber:", forControlEvents: UIControlEvents.TouchUpInside)
                
                //Distance Calculation: direct Implementation
                let currentLocationInCL = LocationManagerDelegate.sharedInstance.currentLocation
                let currentLocation : PFGeoPoint? = PFGeoPoint(location: currentLocationInCL)
                let postLocation = self.bureauOffers[indexPath.row]["location"] as? PFGeoPoint
                if currentLocation != nil && postLocation != nil {
                    cell.distanceLabel.text = getDistanceString(postLocation!.distanceInKilometersTo(currentLocation))
                }
            }
            return cell
        case 1:
            let cell:OfferCell = self.tableView.dequeueReusableCellWithIdentifier("offerCell") as! OfferCell
            if (offers.count > 0) {
                
                //Style
                cell.hasLabel.layer.borderWidth = 1
                cell.hasLabel.layer.borderColor = SWAPP_BLUE.CGColor
                cell.wantsLabel.layer.borderWidth = 1
                cell.wantsLabel.layer.borderColor = SWAPP_BLUE.CGColor
                cell.cellBackground.layer.cornerRadius = CORNER_RADIUS
                cell.cellBackground.layer.masksToBounds = true
                
                
                cell.bindData(self.offers[indexPath.row])
                cell.userButton.tag = indexPath.row
                cell.userButton.addTarget(self, action: "gotoProfile:", forControlEvents: UIControlEvents.TouchUpInside)
                
                
                //Distance Calculation: direct Implementation
                let currentLocationInCL = LocationManagerDelegate.sharedInstance.currentLocation
                let currentLocation : PFGeoPoint? = PFGeoPoint(location: currentLocationInCL)
                let postLocation = self.offers[indexPath.row]["location"] as? PFGeoPoint
                if currentLocation != nil && postLocation != nil {
                    cell.distanceLabel.text = getDistanceString(postLocation!.distanceInKilometersTo(currentLocation))
                }
            }
            return cell
        default:
            let cell:UITableViewCell = self.tableView.dequeueReusableCellWithIdentifier("")! as UITableViewCell
            return cell
        }
    }
    
    func callNumber(sender: UIButton) {
        let index = sender.tag
        var number = bureauOffers[index]["phoneNumber"] as! String
        number = number.stringByReplacingOccurrencesOfString(" ", withString: "")
        // call the number indicated by cell
        if let phoneCallURL:NSURL = NSURL(string: "tel://\(number)") {
            let application:UIApplication = UIApplication.sharedApplication()
            if (application.canOpenURL(phoneCallURL)) {
                application.openURL(phoneCallURL);
            }
        }
    }
    

    
    
    // Selection
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        switch indexPath.section {
        case 0:
            break
        case 1:
            let offer = self.offers[indexPath.row] as PFObject
            guard offer["user"] != nil else {
                ProgressHUD.showError("User deleted")
                return()
            }
            let user2 : PFUser = offer["user"] as! PFUser
            let user1 = PFUser.currentUser()
            if user2 != user1 {
                let groupId = Messages.startPrivateChat(user1!, user2: user2)
                selectedCounterPart = user2
                self.openChat(groupId)
            }
            break
        default:
            break
        }
    }
    
    // Edition
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        switch indexPath.section {
        case 0:
            return false
        case 1:
            if let u = self.offers[indexPath.row]["user"] as? PFUser {
                if let cu = PFUser.currentUser() {
                    //print(u.objectId)
                    //print(cu.objectId! as String)
                    if u.objectId! as String == cu.objectId! as String {
                        //print("can edit")
                        return true
                    } else {
                        //print("cannot edit")
                        return false
                    }
                } else { return false }
            } else { return false }
        default:
            return false
        }
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.section{
        case 0:
            break
        case 1:
            if editingStyle == .Delete {
                self.offers[indexPath.row]["status"] = OFFER_STATUS_DELETED
                self.offers[indexPath.row].saveInBackgroundWithBlock({ (success, error) -> Void in
                    if error == nil {
                        print("Succesfully deleted offer")
                        self.offers.removeAtIndex(indexPath.row)
                        tableView.beginUpdates()
                        // Delete the row from the data source
                        self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                        self.tableView.endUpdates()
                    } else { print("error deleting") }
                })
            }
            break
        default:
            break
        }
        
    }
    
    //MARK: -System --------------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        let currentUser = PFUser.currentUser()
        if currentUser != nil {
            if currentUser![PF_USER_ACTIVATED] as? Bool == true {
            } else {
                Utilities.logout(self)
            }
        } else {
            Utilities.logout(self)
        }
    }

    
    // MARK: - Navigation --------------------------------------------------------------
    func gotoProfile(sender: UIButton) {
        let index = sender.tag
        //print("go to Profile of user in cell \(index)")
        performSegueWithIdentifier("gotoProfileSegue", sender: index)
    }
    
    func openChat(groupId: String) {
        self.performSegueWithIdentifier("exchangeChatSegue", sender: groupId)
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "exchangeChatSegue" {
            let chatVC = segue.destinationViewController as! ChatViewController
            chatVC.hidesBottomBarWhenPushed = true
            let groupId = sender as! String
            chatVC.groupId = groupId
            chatVC.counterPart = selectedCounterPart
        }
        if segue.identifier == "gotoProfileSegue" {
            let profileVC = segue.destinationViewController as! ProfileViewController
            let index = sender as! Int
            let user = offers[index]["user"] as! PFUser
            profileVC.user2 = user
        }
    }
    
    
    // MARK: - Empty Results ---------------------------------------------------------------
    
    
    
    @IBAction func shareSearchInFacebook(sender: AnyObject) {
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook) {
            let facebookSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            var sharingText =  ""
            
            if let h1 = searchAmountFrom , let h2 = searchAmountTo {
               sharingText = "I have \(h1) \(searchCurrencyFrom) and I need \(h2) \(searchCurrencyTo)."
            }
            facebookSheet.setInitialText(sharingText + " Would you like to exchange currency with me? Download SwApp in www.getswapp.xyz")
            self.presentViewController(facebookSheet, animated: true, completion: nil)
        } else {
            ProgressHUD.showError("Please configure Facebook in your Device", interaction: true)
        }
    }
    
    
    @IBAction func shareSearchInTwitter(sender: AnyObject) {
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter) {
            let twitterSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            var sharingText =  ""
            
            if let h1 = searchAmountFrom , let h2 = searchAmountTo {
                sharingText = "I have \(h1) \(searchCurrencyFrom) and I need \(h2) \(searchCurrencyTo)"
            }
            twitterSheet.setInitialText(sharingText + " Would you like to exchange currency with me? Download SwApp in www.getswapp.xyz")
            self.presentViewController(twitterSheet, animated: true, completion: nil)
        } else {
            ProgressHUD.showError("Please configure Twitter in your Device", interaction: true)
        }
    }
}