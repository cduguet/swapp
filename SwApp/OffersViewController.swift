////
////  OffersViewController.swift
////  SwApp
////
////  Created by Cristian Duguet on 7/19/15.
////  Copyright (c) 2015 CrowdTransfer. All rights reserved.
////
//
//import UIKit
//
//class OffersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {
//
//    //Da Table
//    @IBOutlet var offersTable: UITableView!
//    @IBOutlet var emptyView: UIView!
//    
//    //Offers List
//    var offers = [PFObject]()
//    var refresher: UIRefreshControl!
//    
//    //--------------------- Search Parameters from SwapViewController -------------
//    var searchCurrencyFrom =  ""
//    var searchCurrencyTo = ""
//    var searchAmountFrom: NSNumber?
//    var searchAmountTo: NSNumber?
//    var searchLocation: PFGeoPoint?
//    
//    // Swapper user from  ProfileViewController
//    var swapper: PFUser?
//    
//    // -------------------------------------------------------------------------------------------
//    
//    // viewDidLoad
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        // --------------- Navigation Bar Style --------------
//        let bar:UINavigationBar! = self.navigationController?.navigationBar
//        let img = getImageWithColor(SWAPP_BLUE, size: CGSizeMake(1, 1))
//        bar.setBackgroundImage(img, forBarMetrics: UIBarMetrics.Default)
//        bar.tintColor = UIColor.blackColor()
//        bar.shadowImage = nil
//
//        refresher = UIRefreshControl()
//
//        refresher?.addTarget(self, action: "refresh", forControlEvents: UIControlEvents.ValueChanged)
//        self.offersTable?.addSubview(refresher!)
//                
//        self.emptyView?.hidden = true
//        ProgressHUD.show("Loading...", interaction: false)
//        updateFeed(searchCurrencyFrom, currencyTo: searchCurrencyTo, amountFrom: searchAmountFrom, location: searchLocation) { ProgressHUD.dismiss() }
//        
//        LocationManagerDelegate.sharedInstance.startUpdatingLocation()
//    }
//
//    func updateEmptyView() {
//        self.emptyView?.hidden = (self.offers.count != 0)
//    }
//    
//    override func viewDidAppear(animated: Bool) {
//        super.viewDidAppear(animated)
//        let currentUser = PFUser.currentUser()
//        if currentUser != nil {
//            if currentUser![PF_USER_ACTIVATED] as? Bool == true {
//            } else {
//                Utilities.logout(self)
//            }
//        } else {
//            Utilities.logout(self)
//        }
//    }
//    
//    func refresh() {
//    updateFeed(searchCurrencyFrom, currencyTo: searchCurrencyTo, amountFrom: searchAmountFrom, location: searchLocation) { self.refresher.endRefreshing() }
//    }
//    
//    // **************************** UPDATE USERS FROM SERVER **********************
//    
//    func updateFeed(currencyFrom: String, currencyTo: String, amountFrom: NSNumber?, location: PFGeoPoint?, completionHandler: (() -> Void)) {
//        downloadOffers(currencyFrom, currencyTo: currencyTo, amountFrom: amountFrom, location: location) { (result: [PFObject]?) -> Void in
//            self.offers.removeAll(keepCapacity: false)
//            self.offers += result!
//            self.offersTable.reloadData()
//            self.updateEmptyView()
//            completionHandler()
//        }
//    }
//    
//    
//    // --------------------------------------------------------------------------------------------------------
//    // Table Stuff
//    
//    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return offers.count
//    }
//    
//    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//        
//        let cell:OfferCell = self.offersTable.dequeueReusableCellWithIdentifier("offerCell") as! OfferCell
//        if (offers.count > 0) {
//            
//            cell.hasLabel.layer.borderWidth = 1
//            cell.hasLabel.layer.borderColor = UIColor.grayColor().CGColor
//            cell.wantsLabel.layer.borderWidth = 1
//            cell.wantsLabel.layer.borderColor = UIColor.grayColor().CGColor
//            
//            cell.bindData(self.offers[indexPath.row])
//            cell.userButton.tag = indexPath.row
//            cell.userButton.addTarget(self, action: "gotoProfile:", forControlEvents: UIControlEvents.TouchUpInside)
//            
//            
//            //direct Implementation
//            let currentLocationInCL = LocationManagerDelegate.sharedInstance.currentLocation
//            let currentLocation : PFGeoPoint? = PFGeoPoint(location: currentLocationInCL)
//            let postLocation = self.offers[indexPath.row]["location"] as? PFGeoPoint
//            if currentLocation != nil && postLocation != nil {
//                cell.distanceLabel.text = "\(postLocation!.distanceInKilometersTo(currentLocation)) kms away"
//            }
//        }
//        return cell
//    }
//    
//    func gotoProfile(sender: UIButton) {
//        let index = sender.tag
//        //print("go to Profile of user in cell \(index)")
//        performSegueWithIdentifier("gotoProfileSegue", sender: index)
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//    
//    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        tableView.deselectRowAtIndexPath(indexPath, animated: true)
//        
//        let offer = self.offers[indexPath.row] as PFObject
//        if offer["user"] == nil {
//            ProgressHUD.showError("User deleted")
//            return()
//        } else {
//            let user2 : PFUser = offer["user"] as! PFUser
//            let user1 = PFUser.currentUser()
//            let groupId = Messages.startPrivateChat(user1!, user2: user2)
//            //print(groupId)
//            self.openChat(groupId)
//        }
//    }
//    
//    
//    
//    // --------------------------------------------------------------------------------------------------------
//    
//    // MARK: - Navigation
//    func openChat(groupId: String) {
//        self.performSegueWithIdentifier("exchangeChatSegue", sender: groupId)
//    }
//    
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        if segue.identifier == "exchangeChatSegue" {
//            let chatVC = segue.destinationViewController as! ChatViewController
//            chatVC.hidesBottomBarWhenPushed = true
//            let groupId = sender as! String
//            chatVC.groupId = groupId
//        }
//        if segue.identifier == "gotoProfileSegue" {
//            let profileVC = segue.destinationViewController as! ProfileViewController
//            let index = sender as! Int
//            let user = offers[index]["user"] as! PFUser
//            profileVC.user2 = user
//        }
//    }
//}
