//
//  ViewController.swift
//  
//
//  Created by Cristian Duguet on 10/25/15.
//
//

import UIKit

class OnboardingViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    
    let pageTitles = ["1. Search for exchanges nearby",
        "2. Choose your best match",
        "3. You can check their profile, to see if they are trustworthy",
        "4. Get in contact with them, meet and exchange!",
        "You can also see their reputations and opinions"]
    
    var images = ["screenshot1.png",
        "screenshot2.png",
        "screenshot3.png",
        "screenshot4.png",
        "screenshot5.png"]
    var count = 0
    
    
    var pageViewController : UIPageViewController!
    
    @IBOutlet var restartButton: UIButton!
    @IBOutlet var skipButton: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        restartButton.layer.borderWidth = 1
        restartButton.layer.cornerRadius = CORNER_RADIUS
        restartButton.layer.masksToBounds = true
        restartButton.layer.borderColor = SWAPP_BLUE.CGColor
            
        skipButton.layer.borderWidth = 1
        skipButton.layer.cornerRadius = CORNER_RADIUS
        skipButton.layer.masksToBounds = true
        skipButton.layer.borderColor = SWAPP_BLUE.CGColor
        
        let pageControl = UIPageControl.appearance()
        pageControl.pageIndicatorTintColor = UIColor.lightGrayColor()
        pageControl.currentPageIndicatorTintColor = UIColor.blackColor()

        //PAge View Controller
        
        /* Getting the page View controller */
        self.pageViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PageViewController") as! UIPageViewController
        self.pageViewController.dataSource = self
        reset()
    }
    
    func reset() {
        let pageContentViewController = self.viewControllerAtIndex(0)
        self.pageViewController.setViewControllers([pageContentViewController!], direction: UIPageViewControllerNavigationDirection.Forward, animated: true, completion: nil)
        
        /* We are substracting 30 because we have a start again button whose height is 30*/
        self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height - 32)
        self.addChildViewController(pageViewController)
        self.view.addSubview(pageViewController.view)
        self.pageViewController.didMoveToParentViewController(self)
    }
    
    @IBAction func start(sender: AnyObject) {
//        let pageContentViewController = self.viewControllerAtIndex(0)
        self.pageViewController.dismissViewControllerAnimated(true, completion: nil)
//        self.pageViewController.setViewControllers([pageContentViewController!], direction: UIPageViewControllerNavigationDirection.Forward, animated: true, completion: nil)
        reset()
    }
 
    
    @IBAction func skip(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
    
        var index = (viewController as! PageContentViewController).pageIndex!
        index++
        if(index == self.images.count){
            skipButton.setTitle("Finish", forState: .Normal)
            return nil
        } else {
            skipButton.setTitle("Skip", forState: .Normal)
        }
        return self.viewControllerAtIndex(index)
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
    
        var index = (viewController as! PageContentViewController).pageIndex!
        if(index <= 0){
            return nil
        }
        index--
        return self.viewControllerAtIndex(index)
    
    }
    
    func viewControllerAtIndex(index : Int) -> UIViewController? {
        if((self.pageTitles.count == 0) || (index >= self.pageTitles.count)) {
            return nil
        }
        let pageContentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PageContentViewController") as! PageContentViewController
    
        pageContentViewController.imageName = self.images[index]
        pageContentViewController.titleText = self.pageTitles[index]
        pageContentViewController.pageIndex = index
    
        
            
        return pageContentViewController
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return pageTitles.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
    return 0
    }

    override func viewWillAppear(animated: Bool) {
        UIApplication.sharedApplication().statusBarStyle = .LightContent
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.destinationViewController.isKindOfClass(TabBarController) {
            print("is this working?")
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
