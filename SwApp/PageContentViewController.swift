//
//  PageContentViewController.swift
//  SwApp
//
//  Created by Cristian Duguet on 10/24/15.
//  Copyright © 2015 CrowdTransfer. All rights reserved.
//

import UIKit

class PageContentViewController: UIViewController {
    @IBOutlet var heading: UILabel!
    @IBOutlet var image: UIImageView!
    
    var pageIndex: Int?
    var titleText: String!
    var imageName: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.image.image = UIImage(named: imageName)
        self.heading.text = self.titleText
        self.heading.alpha = 0.1
        UIView.animateWithDuration(1.0) { () -> Void in
            self.heading.alpha = 1.0
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
