//
//  ProfileViewController.swift
//  SwApp
//
//  Created by Cristian Duguet on 7/19/15.
//  Copyright (c) 2015 CrowdTransfer. All rights reserved.
//

import UIKit
import MessageUI
import ParseUI
import Social
import Cosmos
import JWT


let traityKey = "tI0c1olhMQTJkJORLBM5yg"
let traitySecret = "DKHkMa5fMRvDcoqUZpEFamOy9z2kMlAO74FUw"

class ProfileViewController: UIViewController, UIActionSheetDelegate,
    UINavigationControllerDelegate,
    MFMailComposeViewControllerDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UITableViewDataSource, UITableViewDelegate, UIImagePickerControllerDelegate{
    
    @IBOutlet var table: UITableView!
    @IBOutlet weak var verifiedPassportImage: UIImageView!
    @IBOutlet var verifiedPassportLabel: UILabel!
    @IBOutlet var userImageView: PFImageView!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var optionsButton: UIBarButtonItem!
    @IBOutlet var userMessageLabel: UILabel!
    @IBOutlet var messageButton: UIButton!
    @IBOutlet var messageButtonLabel: UILabel!
    @IBOutlet var userRating: CosmosView!
    @IBOutlet var raters: UIButton!
    @IBOutlet var traityBadgeImage: UIImageView!
    @IBOutlet var traityBadgeLabel: UILabel!
    
    var user2: PFUser? // for accessing profile of other people
    var user1:PFUser!   // Viewer's Profile
    var userID: String = ""
    var userIsTraity = false
    
    var friendsList = [Dictionary<String, String>]()
    
    let accountIcons = [    "facebook"     : "logo-facebook",
        "twitter"      : "logo-twitter",
        "google"       : "logo-google-plus",
        "linkedin"     : "logo-linkedin",
        "airbnb"       : "logo-airbnb",
        "uber"         : "logo-uber",
        "ebay"         : "logo-ebay",
        "amazon"       : "logo-amazon",
        "instagram"    : "logo-instagram",
        "paypal"       : "logo-paypal",
        "angelist"     : "logo-angelist",
        "couchsurfing" : "logo-couchsurfing",
        "passport"     : "passport",
        "smsleft"      : "logo-sms"]
    
    let accountNames = [    "facebook"      :   "Facebook"      ,
        "twitter"       :   "Twitter"       ,
        "google"        :   "Google Plus"   ,
        "linkedin"      :   "LinkedIn"      ,
        "airbnb"        :   "Airbnb"        ,
        "uber"          :   "Uber"          ,
        "ebay"          :   "Ebay"          ,
        "amazon"        :   "Amazon"        ,
        "instagram"     :   "Instagram"     ,
        "paypal"        :   "PayPal"        ,
        "angelist"      :   "Angelist"      ,
        "couchsurfing"  :   "Couchsurfing"  ,
        "passport"      :   "Passport"      ,
        "smsleft"       :   "SMS"           ]
    
    
    // Structure for dynamic and contextual actions 
    
    enum userActionType : String {
        case currentOffers = "currentOffers"
        case myOffers = "myOffers"
        case reviews = "reviews"
        case myReviews = "myReviews"
        case verifyPassport = "verifyPassport"
        case connectToTraity = "connectToTraity"
    }
    
    let actionTitles = [
        userActionType.currentOffers    : "Current Offers",
        userActionType.myOffers         : "My Offers",
        userActionType.reviews          : "Reviews",
        userActionType.myReviews        : "My Reviews",
        userActionType.verifyPassport   : "Verify Passport",
        userActionType.connectToTraity  : "Get a trust badge"]
  
    let actionDescriptions = [
        userActionType.currentOffers    : "See this user's offers",
        userActionType.myOffers         : "Manage your current offers",
        userActionType.reviews          : "See what others think about this user",
        userActionType.myReviews        : "See what people think about you",
        userActionType.verifyPassport   : "People will trust you more",
        userActionType.connectToTraity  : "Connect your social networks to traity"]
    
    let actionIcons = [
        userActionType.currentOffers    : "tab_market",
        userActionType.myOffers         : "tab_market",
        userActionType.reviews          : "thumbs_up",
        userActionType.myReviews        : "thumbs_up",
        userActionType.verifyPassport   : "passport",
        userActionType.connectToTraity  : "traity-big-level-gold"]
    
    let actionSelectors = [
        userActionType.currentOffers    : "Current Offers",
        userActionType.myOffers         : "My Offers",
        userActionType.reviews          : "Reviews",
        userActionType.myReviews        : "My Reviews",
        userActionType.verifyPassport   : "Verify Passport",
        userActionType.connectToTraity  : "connectToTraity"]
    
    // The current actions and Connected accounts the current user have
    var actions: [userActionType] = []
    var connectedAccounts: [String] =  []
    var isObserved = false
    
    
    
    
    //MARK: -ViewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let bar:UINavigationBar! =  self.navigationController?.navigationBar
        bar.tintColor = UIColor.whiteColor()
        
        // Show slider of list of common friends
//        friendsListCollectionView!.dataSource = self
//        friendsListCollectionView!.delegate = self
//        friendsListCollectionView!.registerClass(friendCollectionViewCell.self, forCellWithReuseIdentifier: "friendCollectionViewCell")
//        friendsListCollectionView!.backgroundColor = UIColor.clearColor()
//        self.view.addSubview(friendsListCollectionView!)
        
        // Styling
        userImageView.layer.cornerRadius = CORNER_RADIUS
        userImageView.layer.masksToBounds = true;
        userMessageLabel.layer.cornerRadius = CORNER_RADIUS
        userMessageLabel.layer.masksToBounds = true;
        
        messageButton.layer.cornerRadius = messageButton.frame.size.width / 2
        messageButton.layer.masksToBounds = true
        messageButton.layer.borderColor = UIColor.whiteColor().CGColor
        messageButton.layer.borderWidth = 1
        
        //User rating View
        userRating.didTouchCosmos = { rating in
            self.performSegueWithIdentifier("reputationSegue", sender: self)
        }
        
        // Traity Verification
        verifiedPassportImage.hidden = true
        verifiedPassportLabel.hidden = true
        traityBadgeImage.hidden = true
        traityBadgeLabel.hidden = true
        
        //To not display the extra empty cells
        table.tableFooterView = UIView()
        //To display border
        table.layer.borderWidth = 1.0
        table.layer.borderColor = UIColor.grayColor().CGColor
    }
    
    @IBAction func ratersPressed(sender: AnyObject) {
        self.performSegueWithIdentifier("reputationSegue", sender: self)
    }
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        let currentUser = PFUser.currentUser()
        if currentUser != nil {
            if currentUser![PF_USER_ACTIVATED] as? Bool == true {
                
                //user 2 is set by segue
                //Set the elements for another user
                if user2 != nil {
                    self.navigationItem.rightBarButtonItem = nil
                    messageButton.hidden = false
                    messageButtonLabel.hidden = false
                    //friendsListCollectionView.hidden = true
                    //commonFriendsLabel.hidden = true
                    loadUser(user2)
                    print("loading user 2")
                    userID = user2!.valueForKey(PF_USER_OBJECTID) as! String
                    userIsTraity = user2?.valueForKey(PF_USER_ISTRAITY) as? Bool ?? false
                    actions = [userActionType.currentOffers, userActionType.reviews]
                } else {
                    actions = [userActionType.myOffers, userActionType.myReviews]
                    loadUser(currentUser)
                    messageButton.hidden = true
                    messageButtonLabel.hidden = true
                    userID = currentUser!.valueForKey(PF_USER_OBJECTID) as! String
                    userIsTraity = currentUser?.valueForKey(PF_USER_ISTRAITY) as? Bool ?? false
                }
                table.reloadData()
                
                
            } else {
                Utilities.logout(self)
            }
        } else {
            Utilities.logout(self)
        }
        
    }
    
    
    func loadUser(user: PFUser!) {
        userImageView.file = user[PF_USER_PICTURE] as? PFFile
        userImageView.loadInBackground { (image: UIImage?, error: NSError?) -> Void in
            if error != nil {
                print(error)
            }
        }

        usernameLabel.text = user[PF_USER_FULLNAME] as? String
        userMessageLabel.text = user[PF_USER_MESSAGE] as? String
        
        if userMessageLabel.text == "" || userMessageLabel.text == nil {
            userMessageLabel.text = "\(user[PF_USER_FIRSTNAME]) has not written any description yet"
        }
    
        // Get User Star Rating
        userRating.rating = user[PF_USER_RATING] as? Double ?? 0.0
        let ratersInt = user[PF_USER_RATERS] as? Int ?? 0
        raters.setTitle("(\(ratersInt))", forState: .Normal)
        
        //if friendsList.count == 0 && user[PF_USER_FACEBOOKID] != nil {
        // Facebook: get taggable friends with pictures
        //getFBTaggableFriends(nil, failureHandler: {(error)
        //in            print(error)});
        //
        //Facebook: get mutual friends with pictures
        //
        //    getFBMutualFriends(user[PF_USER_FACEBOOKID] as! String)
        //}
    
        updateReputation(user)
    }
    
    func updateReputation(user: PFUser) {
        // Toggle Traity status
        let isTraity = user[PF_USER_ISTRAITY] as? Bool ?? false
        if !isTraity && user == PFUser.currentUser() {
            actions.append(userActionType.connectToTraity)
        } else {
            actions = actions.filter {$0 != userActionType.connectToTraity}
        }
        
        //print(user)
        if let traityReputation = user[PF_USER_TRAITYBADGE] as? traityBadgeType.RawValue {
            traityBadgeImage.hidden = false
            traityBadgeLabel.hidden = false
            traityBadgeImage.image = UIImage(named: "traity-big-level-\(traityReputation)")
            traityBadgeLabel.text = traityReputation
        } else {
            traityBadgeImage.hidden = true
            traityBadgeLabel.hidden = true
        }
        
        // Toggle Passport Status
        
        let passportState = user[PF_USER_PASSPORTVERIFIED] as? Bool ?? false
        self.verifiedPassportImage.hidden =  !passportState
        self.verifiedPassportLabel.hidden = !passportState
        // TO DO: Add possibility to check passport if the user does not have traity
        if isTraity && !passportState && user == PFUser.currentUser() && !actions.contains(userActionType.verifyPassport){
            actions.append(userActionType.verifyPassport)
        }
        
        if let accounts = user[PF_USER_CONNECTEDACCOUNTS] as? [String] {
            connectedAccounts = accounts
        }
        table.reloadData()
    }
    
    
    
    
    @IBAction func messageButtonPressed(sender: AnyObject) {
        let user1 = PFUser.currentUser()
        let groupId = Messages.startPrivateChat(user1!, user2: user2!)
        print(groupId)
        self.openChat(groupId)
    }
    func openChat(groupId: String) {
        self.performSegueWithIdentifier("exchangeChatSegue", sender: groupId)
    }
    
    func getFBMutualFriends(userID: String) {
        let path = "/" + userID
        //println("userID :" + userID)
        let parameters = ["fields": "context.fields(mutual_friends)"]
        
        let request = FBSDKGraphRequest(graphPath: path, parameters: parameters)
        request.startWithCompletionHandler { (connection : FBSDKGraphRequestConnection!, result : AnyObject!, error : NSError!) -> Void in
            if ((error) != nil)            {
                // Process error
                print("Error: \(error)")
            } else {
                let resultdict = result as! NSDictionary
                if let data  = resultdict.objectForKey("data") as? NSArray {
                    for i in 0..<data.count {
                        let valueDict : NSDictionary = data[i] as! NSDictionary
                        let id = valueDict.objectForKey("id") as! String
                        let name = valueDict.objectForKey("name") as! String
                        let pictureDict = valueDict.objectForKey("picture") as! NSDictionary
                        let pictureData = pictureDict.objectForKey("data") as! NSDictionary
                        let pictureURL = pictureData.objectForKey("url") as! String
                        self.friendsList.append(["name": name, "id": id, "pic": pictureURL])
                        print("Name: \(name)")
                        print("ID: \(id)")
                        print("PictureDict")
                    }
//                    self.friendsListCollectionView.reloadData()
//                    self.commonFriendsLabel.text = "Common Friends (\(self.friendsList.count))"
                } else { /*print("No friends in common with this dude")*/ }
            }
        }
    }
    
    func getFBTaggableFriends(nextCursor : String?, failureHandler: (error: NSError) -> Void) {
        let qry : String = "me/taggable_friends"
//        var parameters = Dictionary<String, String>()
//        if nextCursor == nil {
//            parameters = nil
//        } else {
//            parameters!["after"] = nextCursor
//        }
        
        var parameters = Dictionary<String, String>()
        if nextCursor != nil {
            parameters = ["after": nextCursor!]
        }
        
//        let parameters : Dictionary<String, String>?
//        guard nextCursor != nil  else {
//            parameters = nextCursor!["after"]
//        }
//        
//
        // Facebook: get taggable friends with pictures
        let request = FBSDKGraphRequest(graphPath: qry, parameters: parameters)
        request.startWithCompletionHandler { (connection : FBSDKGraphRequestConnection!, result : AnyObject!, error : NSError!) -> Void in
            if ((error) != nil)
            {
                // Process error
                print("Error: \(error)")
            }
            else
            {
                //println("fetched user: \(result)")
                let resultdict = result as! NSDictionary
                let data : NSArray = resultdict.objectForKey("data") as! NSArray
                
                for i in 0..<data.count {
                    let valueDict : NSDictionary = data[i] as! NSDictionary
                    let id = valueDict.objectForKey("id") as! String
                    let name = valueDict.objectForKey("name") as! String
                    let pictureDict = valueDict.objectForKey("picture") as! NSDictionary
                    let pictureData = pictureDict.objectForKey("data") as! NSDictionary
                    let pictureURL = pictureData.objectForKey("url") as! String
                    self.friendsList.append(["name": name, "id": id, "pic": pictureURL])
                }
                if let _ = ((resultdict.objectForKey("paging") as? NSDictionary)?.objectForKey("cursors") as? NSDictionary)?.objectForKey("after") as? String {
//                    self.getFBTaggableFriends(after, failureHandler: {(error) in
//                    println("error")})
                } else {
                    print("Can't read next!!!")
                }
//                self.friendsListCollectionView.reloadData()
//                self.commonFriendsLabel.text = "Common Friends (\(self.friendsList.count))"
            }
        }
    }
    
    
    // MARK: - User actions
    
    func cleanup() {
        userImageView.image = UIImage(named: "avatar_placeholder")
        usernameLabel.text = nil
        traityBadgeImage.hidden = true
        traityBadgeLabel.hidden = true
        verifiedPassportImage.hidden = true
        verifiedPassportLabel.hidden = true
    }
    
    
    @IBAction func optionsButtonPressed(sender: AnyObject) {
        /* iOS 8 Method */
        if objc_getClass("UIAlertController") != nil {
        
            let ActionSheet =  UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
            ActionSheet.addAction(UIAlertAction(title: "Edit my Profile", style: UIAlertActionStyle.Default, handler:{ action in
                self.performSegueWithIdentifier("editProfileSegue", sender: self) }))
            ActionSheet.addAction(UIAlertAction(title: "Share us in Facebook", style: UIAlertActionStyle.Default, handler:{ action in self.shareFacebook()}))
            ActionSheet.addAction(UIAlertAction(title: "Want a Feature?", style: UIAlertActionStyle.Default, handler:{ action in self.sendFeedback()}))
            //ActionSheet.addAction(UIAlertAction(title: "Share this in Twiter", style: UIAlertActionStyle.Default, handler:{ action in self.shareTwitter()}))
            ActionSheet.addAction(UIAlertAction(title: "Log Out", style: UIAlertActionStyle.Destructive, handler:{ action in self.logOutDialog()}))
            ActionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler:nil))
            self.presentViewController(ActionSheet, animated: true, completion: nil)
        } else {
        /* iOS 7 Method */
            let actionSheet = UIActionSheet(title: nil, delegate: self,
                cancelButtonTitle: "Cancel",
                destructiveButtonTitle: "Edit my Profile",
                otherButtonTitles: "Share us in Facebook",
                "Want a feature?",
                "Log Out"
                //"Share this in Twitter",
                );
            actionSheet.destructiveButtonIndex = 4;
            actionSheet.showInView(self.view);
        }
    }

    // MARK: UIActionSheetDelegate
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        switch buttonIndex {
        case 1:
            print("Edit my Profile");
            self.performSegueWithIdentifier("editProfileSegue", sender: self)
            break;
        case 2:
            print("Share us in Facebook");
            shareFacebook();
            break;
        case 3:
            print("Want a feature?");
            sendFeedback();
            break;
        case 4:
            print("Log Out");
            logOutDialog();
            break;
        default:
            print("Default");
            break;
        }
    }

    func logOutDialog() {
        // iOS 8
        if objc_getClass("UIAlertController") != nil {
            let alertController = UIAlertController(title: NSLocalizedString("Log Out", comment: "Log Out Dialog Title"),
                message: NSLocalizedString("Are you sure you want to log out?", comment: "Log Out Dialog Message"),
                preferredStyle: UIAlertControllerStyle.Alert);
            
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "To Cancel in Alert Dialog"), style: UIAlertActionStyle.Default,handler: nil))
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Log Out", comment: "Log Out Dialog Title"), style: .Default, handler: { action in
                switch action.style{
                case .Default:
                    PFUser.logOut()
                    PushNotication.parsePushUserResign()
                    Utilities.postNotification(NOTIFICATION_USER_LOGGED_OUT)
                    self.cleanup()
                    Utilities.logout(self)
                    break;
                case .Cancel:
                    print("cancel");
                    break;
                default:
                    break;
                }
            }));
            self.presentViewController(alertController, animated: true, completion: nil)
        } else {
            PFUser.logOut()
            PushNotication.parsePushUserResign()
            Utilities.postNotification(NOTIFICATION_USER_LOGGED_OUT)
            self.cleanup()
            Utilities.logout(self)
        }
    }
    
    // Send Feedback
    func sendFeedback() {
        let emailTitle = "Feedback"
        let iOSVersion = UIDevice.currentDevice().systemVersion
        let device = UIDevice.currentDevice().model
        
        let messageBody = "Feature request or bug report? \n \n" +  "iOS Version: " + iOSVersion + "\n" + "Device: " + device
        let toRecipents = ["swappbeta@outlook.com"]
        let mc: MFMailComposeViewController = MFMailComposeViewController()
        mc.mailComposeDelegate = self
        mc.setSubject(emailTitle)
        mc.setMessageBody(messageBody, isHTML: false)
        mc.setToRecipients(toRecipents)
        self.presentViewController(mc, animated: true, completion: nil)
    }
    
    func mailComposeController(controller:MFMailComposeViewController, didFinishWithResult result:MFMailComposeResult, error:NSError?) {
        switch result.rawValue {
        case MFMailComposeResultCancelled.rawValue:
            print("Mail cancelled")
        case MFMailComposeResultSaved.rawValue:
            print("Mail saved")
        case MFMailComposeResultSent.rawValue:
            print("Mail sent")
        case MFMailComposeResultFailed.rawValue:
            print("Mail sent failure: %@", [error!.localizedDescription])
        default:
            break
        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    // Share on Facebook
    func shareFacebook() {
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook) {
            let facebookSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            facebookSheet.setInitialText("Try out SwApp! A way to echange money with others and avoid bank fees! ")
            self.presentViewController(facebookSheet, animated: true, completion: nil)
        } else {
            ProgressHUD.showError("Please configure Facebook in your Device", interaction: true)
        }
    }
    
    // Share in Twitter
    func shareTwitter() {
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter) {
            let twitterSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            twitterSheet.setInitialText("Try out SwApp! A way to echange money with others and avoid bank fees! www.getswapp.xyz")
            self.presentViewController(twitterSheet, animated: true, completion: nil)
        } else {
            ProgressHUD.showError("Please configure Twitter in your Device", interaction: true)
        }
    }
    
    
    
    // Mark: - CollectionView
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return friendsList.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("friendCollectionViewCell", forIndexPath: indexPath) as! friendCollectionViewCell
        //cell.backgroundColor = UIColor.blackColor()
        //cell.textLabel.text = "\(indexPath.section):\(indexPath.row)"
        //cell.userImage.image = UIImage(named: "circle")
        cell.bindData(friendsList[indexPath.item])
        return cell
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "myOffersSegue" {
            let myOffersVC = segue.destinationViewController as! OffersTableViewController
            myOffersVC.hidesBottomBarWhenPushed = true
            if user2 != nil {
                myOffersVC.swapper = user2
            } else {
                myOffersVC.swapper = user1
            }
        } else if segue.identifier == "reputationSegue" {
            let reputationVC = segue.destinationViewController as! ReputationTableViewController
            reputationVC.hidesBottomBarWhenPushed = true
            if user2 != nil {
                reputationVC.swapper = user2
            } else {
                reputationVC.swapper = user1
            }
        } else if segue.identifier == "exchangeChatSegue" {
            let chatVC = segue.destinationViewController as! ChatViewController
            chatVC.hidesBottomBarWhenPushed = true
            let groupId = sender as! String
            chatVC.groupId = groupId
            if user2 != nil { chatVC.counterPart = user2! }
        } else if segue.identifier == "gotoBank"{
            let vc = segue.destinationViewController as! BCHViewController
            vc.hidesBottomBarWhenPushed = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(animated: Bool) {
        // --------------- Navigation Bar Style --------------
        let bar:UINavigationBar! = self.navigationController?.navigationBar
        let img = getImageWithColor(SWAPP_BLUE, size: CGSizeMake(1, 1))
        bar.setBackgroundImage(img, forBarMetrics: UIBarMetrics.Default)
        bar.tintColor = UIColor.whiteColor()
        bar.barStyle = UIBarStyle.Black
        bar.shadowImage = nil
    }
    override func viewWillAppear(animated: Bool) {
        let bar:UINavigationBar! =  self.navigationController?.navigationBar
        bar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        bar.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.0)
        bar.tintColor = UIColor.whiteColor()
        bar.shadowImage = UIImage()
        
        // me
        user1 = PFUser.currentUser()
        //user 2 is set by segue
        if user2 != nil {
            self.navigationItem.rightBarButtonItem = nil
            loadUser(user2)
        } else {
            loadUser(user1)
        }
    }
    
    
    
    ///MARK: -UITable Methods
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = self.table.dequeueReusableCellWithIdentifier("actionCell") as! ActionCell
            cell.title.text = actionTitles[actions[indexPath.row]]
            cell.subtitle.text = actionDescriptions[actions[indexPath.row]]
            cell.actionImage.image = UIImage(named: actionIcons[actions[indexPath.row]]!)
            return cell
        case 1:
            let cell = self.table.dequeueReusableCellWithIdentifier("connectedAccountCell") as! ConnectedAccountCell
            cell.title!.text  = accountNames[connectedAccounts[indexPath.row]]!
            cell.accountImage.image = UIImage(named: accountIcons[connectedAccounts[indexPath.row]]!)
            return cell
        default:
            let cell = self.table.dequeueReusableCellWithIdentifier("connectedAccountsCell")!
            return cell
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return actions.count
        case 1:
            return connectedAccounts.count
        default:
            return 0
        }
    }

    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return nil
        case 1:
            return "Connected Accounts"
        default:
            return ""
        }
    }
    
    //select cells
    func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if indexPath.section == 1 {
            return false
        }
        return true
    }
    
    func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        //print("Selecting cell \(indexPath)")
        switch indexPath.section {
        case 0:
            guard indexPath.row <= actions.count else { return nil }
            switch actions[indexPath.row] {
            
            case userActionType.verifyPassport:
                print("upload Passport")
                let actionSheet =  UIAlertController(title: "Upload Passport", message: "", preferredStyle: .ActionSheet)
                actionSheet.addAction(UIAlertAction(title: "Take Photo", style: .Default, handler: { (action) -> Void in
                    self.uploadPassport(UIImagePickerControllerSourceType.Camera)
                }))
                actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .Default, handler: { (action) -> Void in
                    self.uploadPassport(UIImagePickerControllerSourceType.PhotoLibrary)
                }))
                actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler:nil))
                self.presentViewController(actionSheet, animated: true, completion: nil)
                break

            case userActionType.reviews:
                performSegueWithIdentifier("reputationSegue", sender: self)
                break
                
            case userActionType.myReviews:
                performSegueWithIdentifier("reputationSegue", sender: self)
                break
                
            case userActionType.currentOffers:
                performSegueWithIdentifier("myOffersSegue", sender: self)
                break
            case userActionType.myOffers:
                performSegueWithIdentifier("myOffersSegue", sender: self)
                break
            case userActionType.connectToTraity:
                connectToTraity()
                break
            }
            break
        case 1:
            break
        default:
            break
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        return indexPath
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (section == 0) {
            return 1.0 }
        return 32.0
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    
    
    //UIImagePickerControllerDelegate Method
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        let userID = PFUser.currentUser()?.valueForKey(PF_USER_OBJECTID)
        print("image selected")
        var image2 = image
        
        if image.size.width > 1000 {
            image2 = Images.resizeImage(image, width: 800)!
        }
        let imageData = UIImagePNGRepresentation(image2)
        
        //Encoding
        let base64String = imageData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
        //print(base64String)
        let param = ["passport": base64String]
        
        let manager = SessionManager()
        print("uploading passport...")
        ProgressHUD.show("Uploading Passport")
        manager.PUT("/1.0/app-users/\(userID)/passport", parameters: param, success: { (task, response) -> Void in
            print("Successfully oploaded passport.")
            print(response)
            ProgressHUD.showSuccess("Your passport has been uploaded. It will take a day or two to verify.", interaction: true)
            }, failure: { (task, response) -> Void in
                print(response)
                print("Failed to upload passport.")
                ProgressHUD.showError("Failed to Upload Passport. Please try again.")
        })
        
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    
    
}




/// MARK: - Traity Support

extension ProfileViewController {
    //Traity
    func widget_signature(key: String, secret: String, current_user_id: String, options: [String:AnyObject] = [:]) -> String {
        var payload = options as Dictionary
        payload["time"] = (Int(NSDate().timeIntervalSince1970))
        payload["current_user_id"] = current_user_id
        
        let signature = ["key": key, "payload": JWT.encode(payload, algorithm: .HS256(secret))]
        return JWT.encode(signature, algorithm: .None)
    }
    
    
    //Function only for PFUser.currentUser
    func connectToTraity() {
        // Mark: - Get Access Key if there is no access key saved.
        // NSURLSession Method
        let storage = NSUserDefaults.standardUserDefaults()
        
        if let _ = storage.valueForKey("AccessToken") {}
        else { getAccessToken() }
        //if let expiringDate = storage.valueForKey("TokenExpirationDate"), let refreshToken = storage.valueForKey("RefreshToken") as? String {
        //if date is expired
        //if expiringDate.compare(NSDate()) == NSComparisonResult.OrderedDescending {
        //getAccessToken(true, refreshToken: refreshToken)
        //} else { getAccessToken() }
        //}
        //else { getAccessToken() }
        
        //Mark: -Connect local user to Traity User
        //If the User has not already connected his account to Traity.
        if !userIsTraity {
            let sign = widget_signature(traityKey, secret: traitySecret, current_user_id: userID)
            let redirect_uri = "com.swappinc.SwApp:/oauth2Callback"
            // calculate final url
            let params = "?signature=\(sign)&success=\(redirect_uri)"
            UIApplication.sharedApplication().openURL(NSURL(string: "https://traity.com/apps/connect\(params)")!)
        } else {
            getReputation(PFUser.currentUser()!)
        }
        
        //Mark: -get user reputation
        if !isObserved {
            let _ = NSNotificationCenter.defaultCenter().addObserverForName(
                "AGAppLaunchedWithURLNotification",
                object: nil,
                queue: nil,
                usingBlock: { (notification: NSNotification!) -> Void in
                    ProgressHUD.showSuccess("Successfully connected")
                    self.getReputation(PFUser.currentUser()!)
            })
            isObserved = true
        }
    }
    
    func getReputation(user: PFUser) {
        let manager = SessionManager()
        
        let userID = user.valueForKey(PF_USER_OBJECTID) as! String
        
        manager.GET("/1.0/app-users/\(userID)", parameters: nil, success: { (task, response) -> Void in
            
            //print("Reputation response")
            //print(response)
            //let picAddress = response["picture"] as? String ?? ""
            //Async
            //self.profilePicture.downloadedFrom(link: picAddress, contentMode: UIViewContentMode.ScaleAspectFit)
            
            
            var tempAccounts = [String]()
            // get connected profiles
            if let h = response["verified"] as? [String] {
                for account in h {
                    if let _ = self.accountNames[account] {
                        tempAccounts.append(account)
                    }
                }
            }
            self.connectedAccounts = tempAccounts
            user.setValue(self.connectedAccounts, forKey: PF_USER_CONNECTEDACCOUNTS)
            
            
            print("number of connected profiles now is \(self.connectedAccounts.count)")
            self.table.reloadData()
            user.setValue(true, forKey: PF_USER_ISTRAITY)
            
            // get badge
            
            if let badgeNumber = response["reputation"] as? Int {
                switch badgeNumber {
                case 1:
                    user.setValue("seed", forKey: PF_USER_TRAITYBADGE)
                    break
                case 2:
                    user.setValue("seed", forKey: PF_USER_TRAITYBADGE)
                    break
                case 3:
                    user.setValue("bronze", forKey: PF_USER_TRAITYBADGE)
                    break
                case 4:
                    user.setValue("silver", forKey: PF_USER_TRAITYBADGE)
                    break
                case 5:
                    user.setValue("gold", forKey: PF_USER_TRAITYBADGE)
                    break
                default:
                    user.setValue("none", forKey: PF_USER_TRAITYBADGE)
                }
            }
            
            // get passport badge
            if self.connectedAccounts.contains("passport") {
                user.setValue(true, forKey: PF_USER_PASSPORTVERIFIED)
            }
            self.updateReputation(user)
            
            
            // Save all
            
            user.saveInBackgroundWithBlock({ (success, error) -> Void in
                if error != nil {
                    print("Could not update user into the server")
                } else if success == true {
                    print("Successfully updated user")
                }
            })
            
            
            }, failure: { (task, response) -> Void in
                //print(response)
                print("Failed to get reputation.")
                // FIXME: Check if the error is the lack of Token
                self.getAccessToken()
        })
        
        
        manager.GET("/1.0/app-users/\(userID)/reviews", parameters: nil, success: { (task, response) -> Void in
            // TODO Finish this
            if let h = response["other"]! {
                //print(h)
            }
            
            }, failure: { (task, response) -> Void in
                //print(response)
                print("Failed to get reviews.")
        })
        
    }
    
    func getAccessToken(refresh: Bool = false, refreshToken: String = "") {
        
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        
        let request = NSMutableURLRequest(URL: NSURL(string: "https://api.traity.com/oauth/token")!)
        //        let request = NSMutableURLRequest(urlString: "https://api.traity.com/oauth/token")
        request.HTTPMethod = "POST"
        
        var dataString = "client_id=\(traityKey)&client_secret=\(traitySecret)&grant_type=client_credentials"
        if refresh {
            // FIXME: not working
            print("refreshing token")
            dataString = "client_id=\(traityKey)&client_secret=\(traitySecret)&refresh_token=\(refreshToken)&grant_type=refresh_token"
        }
        
        let theData = dataString.dataUsingEncoding(NSASCIIStringEncoding, allowLossyConversion: true)
        let task = session.uploadTaskWithRequest(request, fromData: theData) { (data, response, error) -> Void in
            
            // verify data return
            guard (error == nil ) else { print("There is an error in the request"); return }
            guard (data != nil) else { print("error catching data"); return }
            
            //process return
            let jsonResult : AnyObject?
            do {
                jsonResult = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)
            } catch { print("Error parsing JSON") ; return }
            guard jsonResult != nil else { print("No results returned. Could not save to memory."); return }
            
            //process token
            //print(jsonResult)
            guard let token = jsonResult!.objectForKey("access_token") as? String else {
                print("Error Unwrapping"); return
            }
            //save token
            let storage = NSUserDefaults.standardUserDefaults()
            storage.setObject(token, forKey: "AccessToken")
            
            if let expirationSeconds =  jsonResult!.objectForKey("expires_in") as? Double {
                let tokenExpirationDate = NSDate(timeIntervalSinceNow: expirationSeconds)
                storage.setObject(tokenExpirationDate, forKey: "TokenExpirationDate")
            }
            if let refreshToken =  jsonResult!.objectForKey("refresh_token") as? String {
                storage.setObject(refreshToken, forKey: "RefreshToken")
            }
        }
        task.resume()
    }
    
    // Upload Passport
    func uploadPassport(source: UIImagePickerControllerSourceType =  .Camera) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = source
        imagePicker.allowsEditing = false
        
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }
}

enum traityBadgeType : String {
    case seed = "seed"
    case bronze = "bronze"
    case silver = "silver"
    case gold = "gold"
    case none = "none"
}





