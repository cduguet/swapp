////
////  RatesManager.swift
////  SwApp
////
////  Created by Cristian Duguet on 9/28/15.
////  Copyright © 2015 CrowdTransfer. All rights reserved.
////
//
//import Foundation
//
//
//class RatesManager {
//    
//    //SINGLETON
//    static let sharedInstance = RatesManager()
//    
//    // Variables from Persistent Storage
//    var ratesDictionary: NSDictionary? = nil
//    var ratesLastUpdate: NSDate? = nil
//    var isUpdated:  Bool
//    
//    private init() {
//        isUpdated = false
//    }
//    
//    //MARK: -Update Rates if necessary
//    func updateRates(force: Bool) {
//        //Pass by value and not by reference
//        ratesDictionary = NSUserDefaults.standardUserDefaults().objectForKey("rates")?.mutableCopy() as? NSMutableDictionary
//        //ratesDictionary = NSUserDefaults.standardUserDefaults().objectForKey("rates") as? NSDictionary
//        ratesLastUpdate = NSUserDefaults.standardUserDefaults().objectForKey("ratesLastUpdate") as? NSDate
//        
//        if  ((ratesDictionary != nil )  && ( ratesLastUpdate?.timeIntervalSinceNow > (-60*60)) && ratesDictionary?.count > 1 ) {
//            print("Rates are less than an hour old")
//            isUpdated = true
//        } else {
//            isUpdated = false
//            print("Downloading and Updating rate values...")
//            downloadRates({ () in
//                // create a ratesDictionary as a copy (not pointer) to the Local Persistent Memory Dictionary, because we're allowing the user to edit the rates,
//                // and we must be able to reset the values to the originals
//                //TODO: delete
//                print("Rates:")
//                print(self.ratesDictionary)
//                self.isUpdated = true
//            })
//        }
//    }
//    
//    // MARK: - Reset Rates
//    func resetRates() -> Bool {
//        guard ratesDictionary != 0 else {print("There is no value to reset in ratesDictionary"); return false }
//        
//        ratesDictionary!.delete(self)
//        //Pass by value and not by reference
//        ratesDictionary = NSUserDefaults.standardUserDefaults().objectForKey("rates")?.mutableCopy() as? NSMutableDictionary
//        //ratesDictionary = NSUserDefaults.standardUserDefaults().objectForKey("rates") as? NSDictionary
//        ratesLastUpdate = NSUserDefaults.standardUserDefaults().objectForKey("ratesLastUpdate") as? NSDate
//        
//        return true
//    }
//    
//    
//    // MARK: - Download Rates MEthod
//    typealias emptyClosure = (() -> Void)?
//    func downloadRates(completionHandler: (emptyClosure)) {
//        var statement = "select * from yahoo.finance.xchange where pair in (\"USD\", \"EUR\", \"ARS\", \"AUD\", \"BRL\", \"CAD\", \"CLP\", \"CNY\", \"COP\", \"GBP\", \"INR\")"
//        for s in CURRENCIES_GLOBAL {
//           statement += "\""+s+"\", "
//        }
//        
//        // FIXME
//        statement.removeAtIndex(statement.endInde)
//        
//        
//        
//        //let prefix:NSString = "https://query.yahooapis.com/v1/public/yql?&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=&q="
//        let prefix:NSString =   "https://query.yahooapis.com/v1/public/yql?&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&q="
//        
//        //build query URL
//        let escapedStatement = statement.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())
//        let query = "\(prefix)\(escapedStatement!)"
//        
//        //NSURL Session approach (asynchronous)
//        let url = NSURL(string: query)
//        let session = NSURLSession.sharedSession()
//        
//        let task = session.dataTaskWithURL(url!, completionHandler: { (data, response, error) -> Void in
//            if (error != nil) {
//                print(error)
//                ProgressHUD.showError("\(error!.userInfo)")
//            } else {
//                
//                let jsonResult: AnyObject?
//                do {
//                    jsonResult =  try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary
//                } catch {
//                    print("Error parsing JSON")
//                    return
//                }
//                guard jsonResult != nil else {
//                    print("No results returned. Could not save to memory.", terminator: "")
//                    return
//                }
//                
//                let arr = ((jsonResult!.objectForKey("query"))!.objectForKey("results"))!.objectForKey("rate") as! NSArray
//                let numberFormatter = NSNumberFormatter()  //for converting to NSNumber
//                var _: NSNumber ; var curr: String
//                // ------------ create rates dictionary ---------------
//                var ratesDictionary  = [String: NSNumber]()
//                //println(arr)
//                for elem in arr {
//                    if let val = numberFormatter.numberFromString(elem.objectForKey("Rate")! as! NSString as String) {
//                        curr = (elem.objectForKey("Name") as! NSString).substringFromIndex(4) as String
//                        //print("\(curr): \(val)")
//                        ratesDictionary[curr] = val
//                    } else {
//                        let error  = "Rates have no numeral values!"
//                        print(error)
//                    }
//                }
//                
//                //---------------------- obtain date-------------------------------------
//                var dateString = ((jsonResult!.objectForKey("query"))!.objectForKey("created")) as! String
//                dateString = dateString.stringByReplacingOccurrencesOfString("T", withString:" ")
//                let index: String.Index = dateString.startIndex.advancedBy(19)
//                dateString = dateString.substringToIndex(index)
//                //println(dateString)
//                
//                let dateFormatter = NSDateFormatter()
//                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss -0ZZZ"
//                let ratesDate = dateFormatter.dateFromString(dateString + " +0000")
//                //println(date)
//                
//                // SAVE IN PERSISTENT MEMORY
//                if ratesDate != nil {
//                    NSUserDefaults.standardUserDefaults().setObject(ratesDate, forKey: "ratesLastUpdate")
//                }
//                //println(ratesDictionary)
//                NSUserDefaults.standardUserDefaults().setObject(ratesDictionary, forKey: "rates")
//                
//                print("Succesfully downloaded Rates to Memory")
//                if  completionHandler != nil {
//                    completionHandler?()
//                }
//                
//            }
//        })
//        task.resume()
//    }
//}