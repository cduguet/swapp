//
//  RatesTableViewController.swift
//  SwApp
//
//  Created by Cristian Duguet on 7/19/15.
//  Copyright (c) 2015 CrowdTransfer. All rights reserved.
//

import UIKit

class RatesTableViewController: UITableViewController {

    
    
    //Define rates array which has the rates
    var ratesArray :[(currency: NSString, value: NSNumber)] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // --------------- Navigation Bar Style --------------
        // set bar element
        let bar:UINavigationBar! =  self.navigationController?.navigationBar
        let img = getImageWithColor(SWAPP_BLUE, size: CGSizeMake(1, 1))
        bar.setBackgroundImage(img, forBarMetrics: UIBarMetrics.Default)
        bar.barStyle = UIBarStyle.Black
        bar.tintColor = UIColor.whiteColor()
        bar.shadowImage = nil
        updateRates()
    }
    
    func updateRates() {
        var ratesDictionary =  NSUserDefaults.standardUserDefaults().objectForKey("rates") as? NSDictionary
        var ratesLastUpdate = NSUserDefaults.standardUserDefaults().objectForKey("ratesLastUpdate") as? NSDate
        
        //print( ratesLastUpdate?.timeIntervalSinceNow)

        if  ((ratesDictionary != nil )  && ( ratesLastUpdate?.timeIntervalSinceNow > (-60*60))) {
            print("Updating values with memory")
            // --------------------- empty and update ratesArray-------------------------
            self.ratesArray.removeAll(keepCapacity: true)
            for (myKey,myValue) in ratesDictionary! {
                self.ratesArray += [(currency: myKey as! NSString, value: myValue as! NSNumber)]
            }
            self.tableView.reloadData()
        } else {
            print("Downloading and Updating rate values")
            Utilities.downloadRates({ () in
                // create a ratesDictionary as a copy (not pointer) to the Local Persistent Memory Dictionary, because we're allowing the user to edit the rates,
                // and we must be able to reset the values to the originals
                ratesDictionary = NSUserDefaults.standardUserDefaults().objectForKey("rates") as? NSDictionary
                ratesLastUpdate = NSUserDefaults.standardUserDefaults().objectForKey("ratesLastUpdate") as? NSDate
                print("Dictionary:")
                print(ratesDictionary)
                self.ratesArray.removeAll(keepCapacity: true)
                for (myKey,myValue) in ratesDictionary! {
                    self.ratesArray += [(currency: myKey as! NSString, value: myValue as! NSNumber)]
                }
                self.tableView.reloadData()
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return ratesArray.count
    }
    
    
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) 
        
        //let cell = self.offersTable.dequeueReusableCellWithIdentifier("offerCell") as! RateCell
        let currency = ratesArray[indexPath.row].0 as String
        let amount = Utilities.currencyFormatter(ratesArray[indexPath.row].1 as CGFloat, currency: currency)
        cell.textLabel?.text = "1 USD = " + amount + " " + currency
        return cell
    }
    
   }
