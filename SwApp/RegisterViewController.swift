//
//  RegisterViewController.swift
//  SwApp
//
//  Created by Cristian Duguet on 7/19/15.
//  Copyright (c) 2015 Cristian Duguet. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController, UINavigationControllerDelegate, UITextFieldDelegate {

    @IBOutlet var firstNameField: UITextField!
    @IBOutlet var lastNameField: UITextField!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var passwordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "dismissKeyboard"))
        self.firstNameField.delegate = self
        self.lastNameField.delegate = self
        self.emailField.delegate = self
        self.passwordField.delegate = self
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.firstNameField.becomeFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == self.firstNameField {
            self.lastNameField.becomeFirstResponder()
        } else if textField == self.lastNameField {
            self.emailField.becomeFirstResponder()
        } else if textField == self.emailField {
            self.passwordField.becomeFirstResponder()
        } else if textField == self.passwordField {
            self.register()
        }
        return true
    }

    @IBAction func signUpPressed(sender: AnyObject) {
        self.register()
    }
    
    //MARK: - Register Function
    func register() {
        let firstName = firstNameField.text!
        let lastName = lastNameField.text!
        let email = emailField.text!.lowercaseString
        let password = passwordField.text!
        
        if firstName.characters.count == 0 || lastName.characters.count == 0 {
            ProgressHUD.showError("Name must be set.")
            return
        }
        if password.characters.count == 0 {
            ProgressHUD.showError("Password must be set.")
            return
        }
        if email.characters.count == 0 {
            ProgressHUD.showError("Email must be set.")
            return
        }
        
        ProgressHUD.show("Please wait...", interaction: false)
        
        let user = PFUser()
        user[PF_USER_USERNAME] = email
        //user[PF_USER_PASSWORD] = password
        user.password = password
        user[PF_USER_EMAIL] = email
        user[PF_USER_EMAILCOPY] = email
        user[PF_USER_FIRSTNAME] = firstName
        user[PF_USER_LASTNAME] = lastName
        user[PF_USER_FULLNAME] = firstName + " " + lastName
        user[PF_USER_FULLNAME_LOWER] = user[PF_USER_FULLNAME]!.lowercaseString
        user.signUpInBackgroundWithBlock { (succeeded: Bool, error: NSError?) -> Void in
            if error == nil {
                user[PF_USER_ACTIVATED] = true
                ProgressHUD.dismiss()
                PushNotication.parsePushUserAssign()
                // User needs to verify email address before continuing
                let alertController = UIAlertController(title: "Email address verification",
                    message: "We have sent you an email that contains a link - you must click this link before you can continue.",
                    preferredStyle: UIAlertControllerStyle.Alert
                )
                alertController.addAction(UIAlertAction(title: "OK",
                    style: UIAlertActionStyle.Default,
                    handler: { alertController in
                        PFUser.logOut()
                        self.navigationController?.popViewControllerAnimated(true)})
                )
                // Display alert
                self.presentViewController(alertController, animated: true, completion: nil)
            } else {
                if let userInfo = error?.userInfo {
                    ProgressHUD.showError(userInfo["error"] as! String)
                }
            }
        }
    }
    
    
    // MARK: - Move view when Keyboard is on
    
    func textFieldDidBeginEditing(textField: UITextField) {
        animateViewMoving(true, moveValue: 100)
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        animateViewMoving(false, moveValue: 100)
    }
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:NSTimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = CGRectOffset(self.view.frame, 0,  movement)
        UIView.commitAnimations()
    }
    
    
}
