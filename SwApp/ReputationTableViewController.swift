//
//  ReputationTableViewController.swift
//  SwApp
//
//  Created by Cristian Duguet on 8/16/15.
//  Copyright (c) 2015 CrowdTransfer. All rights reserved.
//

import UIKit

class ReputationTableViewController: UITableViewController {

    var swapper: PFUser?
    var comments = [PFObject]()
    
    
    
    @IBOutlet var addCommentButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // --------------- Navigation Bar Style --------------
        let bar:UINavigationBar! = self.navigationController?.navigationBar
        let img = getImageWithColor(SWAPP_BLUE, size: CGSizeMake(1, 1))
        bar.setBackgroundImage(img, forBarMetrics: UIBarMetrics.Default)
        bar.tintColor = UIColor.whiteColor()
        bar.barStyle = UIBarStyle.Black
        bar.shadowImage = nil
        
        if swapper == nil {
            self.navigationItem.rightBarButtonItem = nil
            swapper = PFUser.currentUser()
        }
                
        updateFeed()
    }

    func updateFeed() {
        let commentsQuery = PFQuery(className: "Comments")
        commentsQuery.whereKey("aboutUser", equalTo: swapper!)
        
        commentsQuery.findObjectsInBackgroundWithBlock {
            (objects: [PFObject]?, error: NSError?) -> Void in
            if error == nil {
                self.comments.removeAll(keepCapacity: false)
                self.comments += objects as [PFObject]!
                self.tableView.reloadData()
                //self.updateEmptyView()
            } else {
                // Log details of the failure
                ProgressHUD.showError("Network error")
                print("Error: \(error) \(error!.userInfo)")
            }
        }
    }
    
    
    @IBAction func addCommentButtonPressed(sender: AnyObject) {
        /* iOS 8 Method */
        let alert = UIAlertController(title: "Add Comment",
            message: nil,
            preferredStyle: UIAlertControllerStyle.Alert)
        
        let good = UIAlertAction(title: "GOOD", style: UIAlertActionStyle.Default) { action in
            let comment = alert.textFields![0] 
            self.addComment(comment.text!, opinion: 5)
        }
        good.enabled = false
        
        let bad = UIAlertAction(title: "BAD", style: UIAlertActionStyle.Destructive) { action in
            let comment = alert.textFields![0] 
            self.addComment(comment.text!, opinion: 1)
        }
        bad.enabled = false
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (_) in }

        alert.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "How was your experience with this user?"
            NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object: textField, queue: NSOperationQueue.mainQueue()) { (notification) in
                good.enabled = textField.text != ""
                bad.enabled = textField.text != ""
            }
        }
        alert.addAction(good)
        alert.addAction(bad)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func addComment(message: String, opinion: Int) {
        let comment = PFObject(className: "Comments")
        comment["author"] = PFUser.currentUser()
        comment["aboutUser"] = swapper
        comment["comment"] = message
        comment["opinion"] = opinion
        comment.saveInBackgroundWithBlock({ (success, error) -> Void in
            if success == false{
                ProgressHUD.showError("Could not Post. Please try again later")
            } else {
                ProgressHUD.showSuccess("Successfully posted!")
                print("successfully posted")
                self.updateFeed()
            }
        })
        print("opinion")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:CommentCell = self.tableView.dequeueReusableCellWithIdentifier("commentCell") as! CommentCell
        if (comments.count > 0) {
            cell.commentLabel.layer.cornerRadius = CORNER_RADIUS
            cell.commentLabel.layer.masksToBounds = true
            cell.bindData(self.comments[indexPath.row])
        }
        return cell
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        let currentUser = PFUser.currentUser()
        if currentUser != nil {
            if currentUser![PF_USER_ACTIVATED] as? Bool == true {
            } else {
                Utilities.logout(self)
            }
        } else {
            Utilities.logout(self)
        }
    }
}
