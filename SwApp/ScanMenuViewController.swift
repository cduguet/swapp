////
////  ScanMenuViewController.swift
////  SwApp
////
////  Created by Cristian Duguet on 10/3/15.
////  Copyright © 2015 CrowdTransfer. All rights reserved.
////
//
//import UIKit
//
//class ScanMenuViewController: UIViewController, PPScanDelegate {
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        // --------------- Navigation Bar Style --------------
//        let bar:UINavigationBar! = self.navigationController?.navigationBar
//        let img = getImageWithColor(SWAPP_BLUE, size: CGSizeMake(1, 1))
//        bar.setBackgroundImage(img, forBarMetrics: UIBarMetrics.Default)
//        bar.tintColor = UIColor.whiteColor()
//        bar.barStyle = UIBarStyle.Black
//        bar.shadowImage = nil
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//    
//    
//    func coordinator() throws -> PPCoordinator? {
//        /** 0. Check if scanning is supported */
//        
//    //        var isUnsupported = false
//    //        
//    //        do { try  isUnsupported = PPCoordinator.isScanningUnsupported()
//    //        } catch let error { throw error }
//    //        
//    //        if isUnsupported {return nil }
//        
//        if let _ = try? PPCoordinator.isScanningUnsupported() { return nil }
//        
//        /** 1. Initialize the Scanning settings */
//        
//        // Initialize the scanner settings object. This initialize settings with all default values.
//        let settings: PPSettings = PPSettings()
//        
//        
//        /** 2. Setup the license key */
//        
//        // Visit www.microblink.com to get the license key for your app
//        settings.licenseSettings.licenseKey = BLINK_LICENSE
//
//        
//        /**
//        * 3. Set up what is being scanned. See detailed guides for specific use cases.
//        * Here's an example for initializing MRTD and USDL scanning
//        */
//        
//        // To specify we want to perform MRTD (machine readable travel document) recognition, initialize the MRTD recognizer settings
//        let mrtdRecognizerSettings: PPMrtdRecognizerSettings = PPMrtdRecognizerSettings()
//
//        
//        // Add MRTD Recognizer setting to a list of used recognizer settings
//        settings.scanSettings.addRecognizerSettings(mrtdRecognizerSettings)
//        
//        // To specify we want to perform USDL (US Driver's license) recognition, initialize the USDL recognizer settings
//        let usdlRecognizerSettings: PPUsdlRecognizerSettings = PPUsdlRecognizerSettings()
//        
//        // Add USDL Recognizer setting to a list of used recognizer settings
//        settings.scanSettings.addRecognizerSettings(usdlRecognizerSettings)
//        
//        
//        /** 4. Initialize the Scanning Coordinator object */
//        
//        let coordinator: PPCoordinator = PPCoordinator(settings: settings)
//        
//        return coordinator
//    }
//    
//    @IBAction func didTapScan(sender: AnyObject) {
////        do { let coord: PPCoordinator = try self.coordinator()! } catch let error { print("error") }
//        let coord : PPCoordinator? = try? self.coordinator()!
//        if coord == nil {
//            let messageString: String = "Cannot show Camera" //error.localizedDescription()
//            UIAlertView(title: "Error", message: messageString, delegate: nil, cancelButtonTitle: "OK").show()
//            return
//        }
//
//        let scanningViewController = coord!.cameraViewControllerWithDelegate(self)
//        self.presentViewController(scanningViewController, animated: true, completion: nil)
//    }
//    
//    //MARK: -PPSCANDELEGATE Methods 
//    
//    func scanningViewControllerUnauthorizedCamera(scanningViewController: UIViewController!) {
//        // Add any logic which handles UI when app user doesn't allow usage of the phone's camera
//
//    }
//    
//    func scanningViewController(scanningViewController: UIViewController!, didFindError error: NSError!) {
//        // Can be ignored. See description of the method
//    }
//    
//    func scanningViewControllerDidClose(scanningViewController: UIViewController!) {
//        // As scanning view controller is presented full screen and modally, dismiss it
//        self.dismissViewControllerAnimated(true, completion: nil)
//    }
//    func scanningViewController(scanningViewController: UIViewController!, didOutputResults results: [AnyObject]!) {
//        // Here you process scanning results. Scanning results are given in the array of PPRecognizerResult objects.
//        
//        // first, pause scanning until we process all the results
//        (scanningViewController as! PPScanningViewController).pauseScanning()
//        // Collect data from the result
//        for result in results {
//            if result.isKindOfClass(PPMrtdRecognizerResult) {
//
//                ProgressHUD.show("Checking User Data")
//
//                let mrtdResult: PPMrtdRecognizerResult = result as! PPMrtdRecognizerResult
////                let title = "MRTD"
////                let message = mrtdResult.description
////                print(mrtdResult.primaryId())
////                print(mrtdResult.secondaryId())
////                print(mrtdResult.issuer())
////                print(mrtdResult.documentNumber())
////                print(mrtdResult.documentCode())
////                print(mrtdResult.dateOfExpiry())
////                print(mrtdResult.dateOfBirth())
////                print(mrtdResult.nationality())
////                print(mrtdResult.sex())
////                print(mrtdResult.opt1())
////                print(mrtdResult.opt2())
//
//                assignUserData(mrtdResult)
//                
//            }
//            else if result.isKindOfClass(PPUsdlRecognizerResult) {
//                //let usdlResult: PPUsdlRecognizerResult = result as! PPUsdlRecognizerResult
//                //let title = "USDL"
//                //let message = usdlResult.description
//                ProgressHUD.showError("Sorry, in the next few weeks we will recognize US drivers license", interaction: true)
//            }
//        }
//
//        self.dismissViewControllerAnimated(true, completion: nil)
//    }
//    
//    func assignUserData(data: PPMrtdRecognizerResult) {
//        let doc = PFObject(className: "Document")
//        doc[ID_FIRSTNAME]                       =  data.secondaryId()
//        doc[ID_LASTNAME]                        = (data.primaryId())
//        doc[ID_ISSUER]                          =         (data.issuer())
//        doc[ID_EXPIRATION]                      =         (data.dateOfExpiry())
//        doc[ID_BIRTH]                           =         (data.dateOfBirth())
//        doc[ID_NATIONALITY]                     =         (data.nationality())
//        doc[ID_SEX]                             =         (data.sex())
//        doc[ID_CODE]                            =        (data.documentCode())
//
//        
//        if doc[ID_CODE] as! String == "ID" {
//            doc[ID_NUMBER]                          =         (data.documentNumber())
//            doc[ID_SERIAL]                          =        (data.opt2()).stringByReplacingOccurrencesOfString("<", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
//        } else if doc[ID_CODE] as! String == "IN" {
//            doc[ID_SERIAL]                          =         (data.documentNumber())
//            doc[ID_NUMBER]                          =        (data.opt2()).stringByReplacingOccurrencesOfString("<", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
//        }
//        
//        checkValidId(doc) { (isValid) -> Void in
//            doc.setObject(isValid, forKey: "valid")
//        }
//        checkSII(doc) { (giros) -> Void in
//            doc.setObject(giros, forKey: "giros")
//            doc.saveEventually()
//        }
//    
//        // Banco de Chie API
//        //print("** BANCO DE CHILE")
//        //BCH_API("/cliente/productos", request: ["apiID": "27_SWP", "rut": doc[ID_NUMBER] ]) { (header, body) -> Void in
//        //    guard header["code"] as! Int == 0 else {
//        //        let msg = header["message"]
//        //        print("El usuario no es cliente.")
//        //        return
//        //    }
//        //    print("El usuario ya es cliente Banco de Chile ")
//        //}
//        
//        // Here, we will be assuming that the user class not verified. If it was verified, it would have not come to this viewcontroller
//        doc.saveInBackgroundWithBlock({ (success, error) -> Void in
//            if success == false{
//                ProgressHUD.showError("Could not save User Info. Please try again later")
//                return
//            } else {
//                if let user = PFUser.currentUser() {
//                    user.setObject(doc, forKey: "document")
//                    user.setObject(true, forKey: "isVerified")
//                    user.saveInBackgroundWithBlock({ (succeeded: Bool, error: NSError?) -> Void in
//                        if error == nil {
//                            if true { //FIXME: REG CIVIL SERVICE TOO SLOW!
//                                ProgressHUD.showSuccess("Succesfully verified! ")
//                                self.navigationController?.popViewControllerAnimated(true)
//                                NSNotificationCenter.defaultCenter().postNotificationName("didVerify", object: self, userInfo: nil)
//                            } //else {
//                              //  ProgressHUD.showSuccess("Verification failed!")
//                            //}
//                        } else {
//                            if let _ = error?.userInfo {
//                                ProgressHUD.showError("Could not save User Info. Please try again later")
//                                return
//                            }
//                        }
//                    })
//                }
//            }
//        })
//    }
//}
