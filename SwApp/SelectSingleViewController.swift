//
//  SelectSingleViewController.swift
//  SwApp
//
//  Created by Cristian Duguet on 7/19/15.
//  Copyright (c) 2015 CrowdTransfer. All rights reserved.
//

import UIKit

protocol SelectSingleViewControllerDelegate {
    func didSelectSingleUser(user: PFUser)
}

class SelectSingleViewController: UITableViewController, UISearchBarDelegate  {

    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var emptyView: UIView!
    
    var users = [PFUser]()
    var delegate: SelectSingleViewControllerDelegate!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchBar.delegate = self
        self.loadUsers()
        //self.emptyView?.hidden = true
        
        // Navigation Bar
        let bar:UINavigationBar! = self.navigationController?.navigationBar
        let img = getImageWithColor(SWAPP_BLUE, size: CGSizeMake(1, 1))
        bar.setBackgroundImage(img, forBarMetrics: UIBarMetrics.Default)
        bar.tintColor = UIColor.whiteColor()
        bar.barStyle = UIBarStyle.Black
        bar.shadowImage = nil

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Backend methods
    
    func loadUsers() {
        let user = PFUser.currentUser()
        let query = PFUser.query()
        if let userID = user?.objectId {
            query!.whereKey(PF_USER_OBJECTID, notEqualTo: userID)
        }
        //query.orderByAscending(PF_USER_FULLNAME)
        //query.limit = 1000
        query!.findObjectsInBackgroundWithBlock { (objects: [PFObject]?, error: NSError?) -> Void in
            if error == nil {
                self.users.removeAll(keepCapacity: false)
                self.users += objects as! [PFUser]!
                self.tableView.reloadData()
                self.updateEmptyView()
                print("number of users: ")
                print(self.users.count)
            } else {
                ProgressHUD.showError("Network error")
            }
        }
    }
    
    func searchUsers(searchLower: String) {
        let user = PFUser.currentUser()
        let query = PFUser.query()
        query!.whereKey(PF_USER_OBJECTID, notEqualTo: user!.objectId!)
        query!.whereKey(PF_USER_FULLNAME_LOWER, containsString: searchLower)
        query!.orderByAscending(PF_USER_FULLNAME)
        query!.findObjectsInBackgroundWithBlock { (objects: [PFObject]?, error: NSError?) -> Void in
            if error == nil {
                self.users.removeAll(keepCapacity: false)
                self.users += objects as! [PFUser]!
                self.tableView.reloadData()
            } else {
                ProgressHUD.showError("Network error")
            }
        }
    }
    
    
    override func viewWillAppear(animated: Bool) {
        self.tableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0)
    }
    
    // MARK: - Helper Methods
    func updateEmptyView() {
        self.emptyView?.hidden = (self.users.count != 0)
    }
    
    // MARK: - User actions
    
    @IBAction func cancelButtonPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return self.users.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) 
        
        let user = self.users[indexPath.row]
        cell.textLabel?.text = user[PF_USER_FULLNAME] as? String
        
        return cell
    }
    
    // MARK: - Table view delegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            if self.delegate != nil {
                self.delegate.didSelectSingleUser(self.users[indexPath.row])
            }
        })
    }
    
    // MARK: - UISearchBar Delegate
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.characters.count > 0 {
            self.searchUsers(searchText.lowercaseString)
        } else {
            self.loadUsers()
        }
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        self.searchBarCancelled()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
    }
    
    func searchBarCancelled() {
        self.searchBar.text = ""
        self.searchBar.resignFirstResponder()
        
        self.loadUsers()
    }    
}

