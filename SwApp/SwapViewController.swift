//
//  SwapViewController.swift
//  SwApp
//
//  Created by Cristian Duguet on 7/19/15.
//  Copyright (c) 2015 CrowdTransfer. All rights reserved.
//

import UIKit

class SwapViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIGestureRecognizerDelegate {
    
    // ****************************** VARIABLES ************************************
    // Variables from Persistent Storage
    var ratesDictionary: NSDictionary? = nil
    var ratesLastUpdate: NSDate? = nil
    
    @IBOutlet var currencyFromField: DropDownTextField!
    @IBOutlet var currencyToField: DropDownTextField!
    @IBOutlet var amountFromField: UITextField!
    @IBOutlet var amountToField: UITextField!
    @IBOutlet var expiringField: DropDownTextField!
    @IBOutlet var matchButton: UIButton!
    @IBOutlet weak var interchangeButton: UIButton!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var changeRateButton: UIButton!
    
    // General variables with numbers
    var amountFromNumber : NSNumber?
    var amountToNumber : NSNumber?
    
    //create location manager
    //var locationManager = CLLocationManager()
    var selectedLocation = PFGeoPoint()
    
    //Initialize Picker Views
    var pickerView1: UIPickerView!
    var pickerView2: UIPickerView!
    var pickerView3: UIPickerView!
    
    //Currency Picker List
    let currencyWithFlag = ["🇺🇸 USD", "🇪🇺 EUR", "🇦🇷 ARS", "🇦🇺 AUD", "🇧🇷 BRL", "🇨🇦 CAD", "🇨🇱 CLP", "🇨🇳 CNY", "🇨🇴 COP", "🇬🇧 GBP", "🇮🇳 INR"]
    
    let currency = ["USD", "EUR", "ARS", "AUD", "BRL", "CAD", "CLP", "CNY", "COP", "GBP", "INR"]
    // Expiration Time Picker List
    let expires = ["15 days", "7 days", "3 days", "1 day", "just search"]
    
    //create activity indicator for pause and restore
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    //Aux variable for PickerView Control
    var activeTextField:UITextField?
    var updating:Bool = false
    
    // ************************************** VIEWDIDLOAD **********************************************
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Image in Title above NAvigation Bar
        let titleImage : UIImage = UIImage(named: "swapp_titlebar")!
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageView.contentMode = .ScaleAspectFit
        imageView.image = titleImage
        self.navigationItem.titleView = imageView
        
        matchButton.layer.borderWidth = 2
        matchButton.layer.borderColor = SWAPP_BLUE.CGColor
        matchButton.layer.cornerRadius = CORNER_RADIUS
        matchButton.layer.masksToBounds = true
        
        
        //Download new rates
        updateRates(false)
        
        //MARK: - Styles ----------------------------------------------------------------------------
        amountFromField.delegate = self
        amountFromField.keyboardType = UIKeyboardType.DecimalPad
        amountToField.delegate = self
        amountToField.keyboardType = UIKeyboardType.DecimalPad
        
        // Give Rounded Corners to Input Fields
        //amountFromField.layer.borderWidth = 1
        amountFromField.layer.cornerRadius = CORNER_RADIUS
        amountFromField.layer.masksToBounds = true

        //amountToField.layer.borderWidth = 1
        amountToField.layer.cornerRadius = CORNER_RADIUS
        amountToField.layer.masksToBounds = true
        
        // Give corners to CurrencyAmounts
        var maskPath  = UIBezierPath(roundedRect: currencyFromField.bounds, byRoundingCorners: [UIRectCorner.BottomRight , UIRectCorner.TopRight], cornerRadii: CGSizeMake(CORNER_RADIUS-1.0, CORNER_RADIUS-1.0))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = currencyFromField.bounds
        maskLayer.path  = maskPath.CGPath
        currencyFromField.layer.mask = maskLayer
    
        maskPath  = UIBezierPath(roundedRect: currencyToField.bounds, byRoundingCorners: [UIRectCorner.BottomRight , UIRectCorner.TopRight], cornerRadii: CGSizeMake(CORNER_RADIUS, CORNER_RADIUS))
        let maskLayer2 = CAShapeLayer()
        maskLayer2.frame = currencyToField.bounds
        maskLayer2.path  = maskPath.CGPath
        currencyToField.layer.mask = maskLayer2
        
        
        // Give Rounded Corners to Input Fields
        
        changeRateButton.layer.borderWidth = 1
        changeRateButton.layer.borderColor = SWAPP_BLUE.CGColor
        changeRateButton.layer.cornerRadius = CORNER_RADIUS
        changeRateButton.layer.masksToBounds = true
        
        locationButton.layer.borderWidth = 1
        locationButton.layer.cornerRadius =  CORNER_RADIUS
        locationButton.layer.masksToBounds = true
        
        //MARK: -User Location -----------------------------------------------------------------1
//        locationManager.delegate = self
//        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
//        locationManager.requestWhenInUseAuthorization()
//        locationManager.startUpdatingLocation()
        LocationManagerDelegate.sharedInstance.startUpdatingLocation()
        let mainQueue = NSOperationQueue.mainQueue()
        NSNotificationCenter.defaultCenter().addObserverForName("updatedLocationName", object: nil, queue: mainQueue, usingBlock: { (note) -> Void in
            let locationName = note.userInfo!["locationName"]! as! String
            print(locationName)
            // GCD MAIN THREAD
            dispatch_async(dispatch_get_main_queue(), {
                self.locationButton.setTitle(locationName, forState: UIControlState.Normal)
            })
        })
        
        
        //MARK: -PickerViews
        // -------------------------------- Initialize PickerViews ---------------------------
        // Initialize and configure PickerViews
        pickerView1 = UIPickerView();         pickerView2 = UIPickerView()
        pickerView1.tag = 0         ;         pickerView2.tag = 1
        pickerView1.delegate = self ;         pickerView2.delegate = self
        
        //        pickerView1.backgroundColor = UIColor(red: 120/255, green: 120/255, blue: 30/255, alpha: 0)
        
        pickerView3 = UIPickerView();
        pickerView3.tag = 2         ;
        pickerView3.delegate = self ;
        
        // --------------------------------- Configure PickerViewTextFields -----------------------------
        currencyFromField.delegate = self                   ;        currencyToField.delegate = self
        //currencyFromField.inputAccessoryView = toolbar      ;        currencyToField.inputAccessoryView = toolbar
        self.currencyFromField.inputView = self.pickerView1 ;        self.currencyToField.inputView = self.pickerView2
        
        expiringField.delegate = self
        //expiringField.inputAccessoryView = toolbar
        self.expiringField.inputView = self.pickerView3
        
        //------------------------------ Tap to Select in PickerView ------------------------
        // one for each pickerView
        let gestureRecognizer1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("pickerViewGestureRecognizer:"))
        let gestureRecognizer2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("pickerViewGestureRecognizer:"))
        let gestureRecognizer3: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("pickerViewGestureRecognizer:"))
        
        gestureRecognizer1.delegate = self
        gestureRecognizer2.delegate = self
        gestureRecognizer3.delegate = self

        pickerView1.addGestureRecognizer(gestureRecognizer1)
        pickerView2.addGestureRecognizer(gestureRecognizer2)
        pickerView3.addGestureRecognizer(gestureRecognizer3)
    }
    
    // ************************************* VIEWDIDAPPEAR*********************************************************
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        let currentUser = PFUser.currentUser()
        if currentUser != nil {
            if currentUser![PF_USER_ACTIVATED] as? Bool == true {
            } else {
                print("User not activated!")
                Utilities.logout(self)
            }
        } else {
            Utilities.logout(self)
        }
    }
    
    
    // ****************************** REAL TIME CURRENCY CONVERSION *****************************
    
    
    //only numbers
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == amountFromField || textField == amountToField {
            return string.containsOnlyCharactersIn("01234567890.,")
        } else {
            return true
        }
    }

    @IBAction func amountFromChanged(sender: AnyObject) {
        updateFields("to")
    }
    
    @IBAction func amountToChanged(sender: AnyObject) {
        updateFields("from")
    }
    
    // Update Exchange Fields 
    
    func updateFields(updateField: String) -> Bool {
        if ratesDictionary != nil {
            let currFrom = currencyFromField.text! //.substringFromIndex(currencyFromField.text!.endIndex.advancedBy(-2))
            let currTo = currencyToField.text! //.substringFromIndex(currencyToField.text!.endIndex.advancedBy(-2))

            if updateField == "to" {
                if amountFromField.text == "" {
                    amountToField.text = ""
                    return true
                }
                let numberFormatter = NSNumberFormatter()  //for converting to NSNumber
                if let val = numberFormatter.numberFromString(amountFromField.text! as String) as? CGFloat {
                    // ----------- Do the Conversion and Print ------------
                    amountToField.text = conversion(val, currency1: currFrom, currency2: currTo, noFormat: false)
                    return true
                }
            } else if updateField == "from" {
                if amountToField.text == "" {
                    amountFromField.text = ""
                    return true
                }
                let numberFormatter = NSNumberFormatter()
                if let val = numberFormatter.numberFromString(amountToField.text! as String) as? CGFloat {
                    amountFromField.text = conversion(val, currency1: currTo, currency2: currFrom, noFormat: false)
                    return true
                }
            } else { return false }
        } else {
            ProgressHUD.showError("No rates loaded")
            return false
        }
        return false
    }
    
    // Do the Currency Conversion
    func conversion(amount1: CGFloat, currency1: String, currency2: String, noFormat: Bool) -> String {
        if ratesDictionary != nil && ratesDictionary?.count >= 1 {
            let rate1 = ratesDictionary!.objectForKey(currency1)! as! NSNumber as CGFloat
            let rate2 = ratesDictionary!.objectForKey(currency2)! as! NSNumber as CGFloat
            let amount2 = amount1 / rate1 * rate2
            if noFormat {
                return Utilities.currencyFormatter(amount2, currency: "RAW")
            } else { return Utilities.currencyFormatter(amount2, currency: currency2) }
        } else {
            ProgressHUD.showError("No currency data, loading...")
            updateRates(false)
            return ""
        }
    }
    
    // **********************************  PERSONALIZE CURRENCY RATE **********************************
    
    func updateExchangeRateLabel() {
        let curr1 = currencyFromField.text!
        let curr2 = currencyToField.text!
        let title = "1 \(curr1) = \(conversion(1, currency1: curr1, currency2: curr2, noFormat: true)) \(curr2)"
        changeRateButton.setTitle(title, forState: .Normal)
    }
    
    @IBAction func interchangeButton(sender: AnyObject) {
        let amountHelper = amountFromField.text
        let currencyHelper = currencyFromField.text
        amountFromField.text = amountToField.text
        currencyFromField.text = currencyToField.text
        amountToField.text = amountHelper
        currencyToField.text = currencyHelper
        updateExchangeRateLabel()
        self.view.endEditing(true)
    }
    
    @IBAction func changeRate(sender: AnyObject) {
        // include exception for same rate
        let curr1 = currencyFromField.text! as String
        let curr2 = currencyToField.text! as String

        /* iOS 8 Method */
        let alertController = UIAlertController(title: "Custom Rate", message: "add your own rate", preferredStyle: UIAlertControllerStyle.Alert)
        
        
        let change = UIAlertAction(title: "change", style: UIAlertActionStyle.Default) { action in
            let newRateField = alertController.textFields![0]
            newRateField.keyboardType = UIKeyboardType.DecimalPad
            
            let numberFormatter = NSNumberFormatter()  //for converting to NSNumber
            if let val = numberFormatter.numberFromString(newRateField.text! as String) as? CGFloat {
                if self.ratesDictionary != nil && val != 0 {
                    let newRate = (self.ratesDictionary!.objectForKey(curr2) as! CGFloat) / val
                    self.ratesDictionary!.setValue(newRate, forKey: curr1)
                    self.updateExchangeRateLabel()
                } else { return }
            }
        }
        
        let reset = UIAlertAction(title: "reset", style: .Default) {action in
            self.updateRates(true)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (_) in }

        alertController.addTextFieldWithConfigurationHandler { (textField) in
            textField.text = self.conversion(1, currency1: curr1, currency2: curr2, noFormat: true)
            
            NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object: textField, queue: NSOperationQueue.mainQueue()) { (notification) in
                change.enabled = textField.text != ""
            }
        }
        alertController.addAction(change)
        alertController.addAction(reset)
        alertController.addAction(cancelAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    
    // ********************************************* UPDATE RATES *************************************************************

    
    func updateRates(force: Bool) {
        //Pass by value and not by reference
        ratesDictionary = NSUserDefaults.standardUserDefaults().objectForKey("rates")?.mutableCopy() as? NSMutableDictionary
        //ratesDictionary = NSUserDefaults.standardUserDefaults().objectForKey("rates") as? NSDictionary
        ratesLastUpdate = NSUserDefaults.standardUserDefaults().objectForKey("ratesLastUpdate") as? NSDate

        if  ((ratesDictionary != nil )  && ( ratesLastUpdate?.timeIntervalSinceNow > (-60*60)) && ratesDictionary?.count > 1 ) {
            print("Rates are less than an hour old")
        } else {
            print("Downloading and Updating rate values...")
            Utilities.downloadRates({ () in
                // create a ratesDictionary as a copy (not pointer) to the Local Persistent Memory Dictionary, because we're allowing the user to edit the rates, 
                // and we must be able to reset the values to the originals
                self.ratesDictionary = NSUserDefaults.standardUserDefaults().objectForKey("rates")?.mutableCopy() as! NSMutableDictionary
                self.ratesLastUpdate = NSUserDefaults.standardUserDefaults().objectForKey("ratesLastUpdate") as? NSDate
                print("Dictionary:")
                //print(self.ratesDictionary)
                self.updateExchangeRateLabel()
            })
        }
    }

    // ********************************************* FIND MATCH *****************************************************************
    @IBAction func findMatch(sender: AnyObject) {
        var error = ""
        let numberFormatter = NSNumberFormatter()  //for converting to NSNumber
        
        let amountFromText = amountFromField.text!.stringByReplacingOccurrencesOfString(",", withString: "")
        let amountToText = amountToField.text!.stringByReplacingOccurrencesOfString(",", withString: "")
        
        selectedLocation = PFGeoPoint(location: LocationManagerDelegate.sharedInstance.currentLocation)
        
        // ----------------- get amounts in NSNumber ------------
        amountFromNumber = numberFormatter.numberFromString(amountFromText as String)
        amountToNumber = numberFormatter.numberFromString(amountToText as String)
        
        // ----------------- check if theres text --------------------
        
        if (amountFromField.text == "") {
            error = "Please insert the amount you have"
        } else if (amountToField.text == "") {
            error = "Please insert the amount you need"
        } else if amountFromNumber == nil || amountToNumber == nil {
            error = "Please enter valid numbers"
        } else if amountFromNumber == 0 {
            error = "Please enter non-zero values"
        }
        
        // display error
        if (error != "") {
            ProgressHUD.showError(error)
        } else {
            pause()
            
            // ----------------- Set expiration date------------------
            var daysToExpire = 0
            
            if expiringField.text == "15 days" {
                daysToExpire = 15
            } else if expiringField.text == "7 days" {
                daysToExpire = 7
            } else if expiringField.text == "3 days" {
                daysToExpire = 3
            } else if expiringField.text == "1 day" {
                daysToExpire = 1
            } else { daysToExpire = 0
            }
            
            //var now = NSDate()
            let userCalendar = NSCalendar.currentCalendar()
            let expiringDate = userCalendar.dateByAddingUnit(
                .Day,
                value: daysToExpire,
                toDate: NSDate(),
                options: [])!
            
            // ---------------- If set an expiration Date, then post --------------------
            if daysToExpire != 0 {
                //Post Currency Exchanges to class "Offers"
                let post = PFObject(className: "Offers")
                //print(PFUser.currentUser())
                
                post["user"] = PFUser.currentUser()
                post["amountFrom"] = amountFromNumber
                post["amountTo"] = amountToNumber
                post["currencyFrom"] = currencyFromField.text
                post["currencyTo"] = currencyToField.text
                post["expiration"] = expiringDate
                post["status"] =  OFFER_STATUS_CREATED
                post["location"] = selectedLocation
                
                post.saveInBackgroundWithBlock({ (success, error) -> Void in
                    if success == false{
                        dispatch_async(dispatch_get_main_queue(), {
                            self.restore()
                            ProgressHUD.showError("Could not Post. Please try again later")
                        })
                    } else {
                        dispatch_async(dispatch_get_main_queue(), {
                            self.restore()
                            ProgressHUD.showSuccess("Successfully posted!")
                        })
                        print("successfully posted")
                        // --------------------- Call SearchSegue ------------------------------------
                        
                        self.performSegueWithIdentifier("searchSegue", sender: self)
                    }
                })
            }
        }
    }

    
    
    //********************************** Function: others ***************************************
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // ****************************** FOR PICKER VIEW **************************************
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int  {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0 {
            return currency.count
        } else if pickerView.tag == 1 {
            return currency.count
        } else if pickerView.tag == 2 {
            return expires.count
        }
        return 1
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0 {
            return currencyWithFlag[row]
        } else if pickerView.tag == 1 {
            return currencyWithFlag[row]
        } else if pickerView.tag == 2 {
            return expires[row]
        }
        
        return ""
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)  {
        
        if pickerView.tag == 0 {
            currencyFromField.text = currency[row]
            updateFields("to")
            updateExchangeRateLabel()
        } else if pickerView.tag == 1 {
            currencyToField.text = currency[row]
            updateFields("to")
            updateExchangeRateLabel()
        } else if pickerView.tag == 2 {
            expiringField.text = expires[row]
            updateExchangeRateLabel()
        }
    }
    
    
    //Implement textFieldDidBeginEditing and store the active UITextField:
    func textFieldDidBeginEditing(textField: UITextField) { // became first responder
        activeTextField = textField
        textField.text = textField.text!.stringByReplacingOccurrencesOfString(",", withString: "")
    }
    // ************************************** PICKERVIEW SELECT BY TAPPING ******************************
    
    // Select element of PickerView by Tapping on it
    func pickerViewGestureRecognizer(gestureRecognizer: UITapGestureRecognizer) {
        let touchPoint : CGPoint =  gestureRecognizer.locationInView(gestureRecognizer.view?.superview)
        let frame: CGRect = self.pickerView2.frame
        print(frame)
        let selectorFrame: CGRect = CGRectInset(frame, 0.0, self.pickerView1.bounds.size.height * 0.83/2)
        if( CGRectContainsPoint( selectorFrame, touchPoint) )
        {
            print("row selected. pressing done")
            activeTextField?.resignFirstResponder()
        }
    }
    
    // Select element of PickerView by Tapping on it
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailByGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    // **************************** END OF FOR PICKER VIEW ***********************************
    
    
    // ******************** Hide Keyboard when tapping outside *******************************
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // ******************************** Segue customization ************************************
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "searchSegue"{
            let vc = segue.destinationViewController as! OffersTableViewController
            vc.searchCurrencyFrom = self.currencyToField.text!
            vc.searchCurrencyTo = self.currencyFromField.text!
            vc.searchAmountFrom = self.amountToNumber
            vc.searchAmountTo = self.amountFromNumber
            vc.searchLocation = self.selectedLocation
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        //let bar:UINavigationBar! =  self.navigationController?.navigationBar
        //bar.setBackgroundImage(nil, forBarMetrics: UIBarMetrics.Default)
        //bar.shadowImage = nil
    }
    
    override func viewWillAppear(animated: Bool) {
        
        let bar:UINavigationBar! = self.navigationController?.navigationBar
        let img = getImageWithColor(SWAPP_BLUE, size: CGSizeMake(1, 1))
        bar.setBackgroundImage(img, forBarMetrics: UIBarMetrics.Default)
        bar.barStyle = UIBarStyle.Black
        bar.tintColor = UIColor.whiteColor()
        //UIApplication.sharedApplication().statusBarStyle = .Default
        bar.shadowImage = nil
    }
    
    
    // ******************************************* FUNCTION: Pause Application *****************************************************
    
    func pause() {
    activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50, 50))
    activityIndicator.center = self.view.center
    activityIndicator.hidesWhenStopped = true
    activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
    view.addSubview(activityIndicator)
    activityIndicator.startAnimating()
    UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    
    // **************** FUNCTION: Restore Application ****************************
    func restore() {
    activityIndicator.stopAnimating()
    UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }
    
    
    //MARK: ---------------------------------------- Location -----------------------------------------------------------------------
//    
//    //************ locationManager : didUpdateLocations ****************
//    // Method: called if location was updated
//    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        
//        let userLocation:CLLocation = locations[0]
//        // USER LATITUDE AND LONGITUDE
//        let latitude = userLocation.coordinate.latitude
//        let longitude = userLocation.coordinate.longitude
//        locationButton.setTitle("\(latitude), \(longitude)", forState: .Normal)
//        
//        //GEOCODER
//        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error)->Void in
//            if (error != nil) {
//                print("Reverse geocoder failed with error" + error!.localizedDescription)
//                return
//            }
//            if placemarks!.count > 0 {
//                let pm = placemarks![0] as CLPlacemark
//                self.displayLocationInfo(pm)
//            } else {
//                print("Problem with the data received from geocoder")
//            }
//        })
//    }
//    
//    //************ displayLocationInfo ****************
//    // update placemark based on Location
//    func displayLocationInfo(placemark: CLPlacemark?) {
//        if let containsPlacemark = placemark {
//            //stop updating location to save battery life
//            locationManager.stopUpdatingLocation()
//            let locality = containsPlacemark.locality ?? ""
//            //let administrativeArea = containsPlacemark.administrativeArea ?? ""
//            let country = containsPlacemark.country ?? ""
//            locationButton.setTitle("\(locality), \(country)", forState: UIControlState.Normal)
//        }
//    }
//    
//    
//    //************ locationManager: didFailWithError ****************
//    // If there is a problem reciving the Location Updates
//    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
//        print("Error while updating location " + error.localizedDescription)
//    }
    
    @IBAction func locationButtonPressed(sender: AnyObject) {
        
    }
    
    
    
}
