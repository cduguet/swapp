//
//  TabBarController.swift
//  SwApp
//
//  Created by Cristian Duguet on 10/27/15.
//  Copyright © 2015 CrowdTransfer. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    var onboard = false
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func unwindToTabBar(segue:UIStoryboardSegue) {
        onboard = true
        self.selectedIndex = 0
    }
    
    override func viewWillAppear(animated: Bool) {
        if onboard {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let onboardingVC = storyboard.instantiateViewControllerWithIdentifier("OnboardingViewController") as! OnboardingViewController
            self.presentViewController(onboardingVC, animated: true, completion: nil)
            print("trying to present Onboarding")
            onboard = false
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
