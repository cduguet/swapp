//
//  Utilities.swift
//  SwApp
//
//  Created by Cristian Duguet on 7/19/15.
//  Copyright (c) 2015 CrowdTransfer. All rights reserved.
//


import Foundation

class Utilities {
    
    
    //Take to Log In
    class func logout(target: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let welcomeVC = storyboard.instantiateViewControllerWithIdentifier("navigationVC") as! UINavigationController
        target.presentViewController(welcomeVC, animated: false, completion: nil)
        
    }

    
    class func postNotification(notification: String) {
        NSNotificationCenter.defaultCenter().postNotificationName(notification, object: nil)
    }
    
    class func timeElapsed(seconds: NSTimeInterval) -> String {
        var elapsed: String
        if seconds < 60 {
            elapsed = "Just now"
        }
        else if seconds < 60 * 60 {
            let minutes = Int(seconds / 60)
            let suffix = (minutes > 1) ? "mins" : "min"
            elapsed = "\(minutes) \(suffix) ago"
        }
        else if seconds < 24 * 60 * 60 {
            let hours = Int(seconds / (60 * 60))
            let suffix = (hours > 1) ? "hours" : "hour"
            elapsed = "\(hours) \(suffix) ago"
        }
        else {
            let days = Int(seconds / (24 * 60 * 60))
            let suffix = (days > 1) ? "days" : "day"
            elapsed = "\(days) \(suffix) ago"
        }
        return elapsed
    }
    
    // Format a String containing certain currency
    class func currencyFormatter(number: CGFloat, currency: String) -> String {
        let formatter = NSNumberFormatter()
        formatter.numberStyle = .DecimalStyle
        if ["USD", "EUR"].contains(currency) { formatter.maximumFractionDigits = 2
        } else if ["RAW"].contains(currency) {
            formatter.maximumFractionDigits = 3
            formatter.minimumSignificantDigits  = 3
        }
        else { formatter.maximumFractionDigits = 0 }
        return formatter.stringFromNumber(number)!
    }
    
    
    class func timeToExpire(seconds: NSTimeInterval) -> String {
        var expiring: String
        if seconds < 60 {
            expiring = "less than a minute"
        }
        else if seconds < 60 * 60 {
            let minutes = Int(seconds / 60)
            let suffix = (minutes > 1) ? "mins" : "min"
            expiring = "\(minutes) \(suffix) left"
        }
        else if seconds < 24 * 60 * 60 {
            let hours = Int(seconds / (60 * 60))
            let suffix = (hours > 1) ? "hours" : "hour"
            expiring = "\(hours) \(suffix) left"
        }
        else {
            let days = Int(seconds / (24 * 60 * 60))
            let suffix = (days > 1) ? "days" : "day"
            expiring = "\(days) \(suffix) left"
        }
        return expiring
    }
    
    
    
    typealias emptyClosure = (() -> Void)?
    class func downloadRates(completionHandler: (emptyClosure)) {
        let statement = "select * from yahoo.finance.xchange where pair in (\"USD\", \"EUR\", \"ARS\", \"AUD\", \"BRL\", \"CAD\", \"CLP\", \"CNY\", \"COP\", \"GBP\", \"INR\")"
        //let prefix:NSString = "https://query.yahooapis.com/v1/public/yql?&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=&q="
        let prefix:NSString =   "https://query.yahooapis.com/v1/public/yql?&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&q="
        
        //build query URL
        let escapedStatement = statement.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())
        let query = "\(prefix)\(escapedStatement!)"
        
//        var jsonResult:NSDictionary? = nil
//        var jsonError:NSError? = nil
        
        //NSURL Session approach (asynchronous)
        let url = NSURL(string: query)
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithURL(url!, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
                dispatch_async(dispatch_get_main_queue(), {
                    ProgressHUD.showError("\(error!.localizedDescription)")
                })
                
            } else {
                
                let jsonResult: AnyObject?
                do {
                    jsonResult =  try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary
                } catch {
                    print("Error parsing JSON")
                    return
                }
                guard jsonResult != nil else {
                    print("No results returned. Could not save to memory.", terminator: "")
                    return
                }
                
                let aux = ((jsonResult!.objectForKey("query"))?.objectForKey("results"))?.objectForKey("rate")
                guard let arr = aux as? NSArray else {
                    print("Error Unwrapping")
                    dispatch_async(dispatch_get_main_queue(), {
                        ProgressHUD.showError("Could not download rates. Please check your Internet connection and try again.", interaction: true)
                    })
                    return
                }
                
                let nf = NSNumberFormatter()  //for converting to NSNumber
                nf.locale = NSLocale(localeIdentifier: "en_US_POSIX")
                var _: NSNumber ; var curr: String
                // ------------ create rates dictionary ---------------
                var ratesDictionary  = [String: NSNumber]()
                //println(arr)
                for elem in arr {
                    if let val = nf.numberFromString(elem.objectForKey("Rate")! as! String) {
                        curr = (elem.objectForKey("Name") as! NSString).substringFromIndex(4) as String
                        //print("\(curr): \(val)")
                        ratesDictionary[curr] = val
                    } else {
                        let error  = "Rates have no numeral values! They are like \(nf.numberFromString(arr[2].objectForKey("Rate")! as! String))"
                        print(error)
                    }
                }
                
                //---------------------- obtain date-------------------------------------
                var dateString = ((jsonResult!.objectForKey("query"))!.objectForKey("created")) as! String
                dateString = dateString.stringByReplacingOccurrencesOfString("T", withString:" ")
                let index: String.Index = dateString.startIndex.advancedBy(19)
                dateString = dateString.substringToIndex(index)
                //println(dateString)
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
                let ratesDate = dateFormatter.dateFromString(dateString)
                //println(date)
                
                // SAVE IN PERSISTENT MEMORY
                if ratesDate != nil {
                    NSUserDefaults.standardUserDefaults().setObject(ratesDate, forKey: "ratesLastUpdate")
                }
                //println(ratesDictionary)
                NSUserDefaults.standardUserDefaults().setObject(ratesDictionary, forKey: "rates")
                
                print("Succesfully downloaded Rates to Memory")
                if  completionHandler != nil {
                    completionHandler?()
                }
                
            }
        })
        task.resume()
    }
    
    
    //transform to dictionary
    func toDictionary<E, K, V>(array: [E], transformer: (E) -> (key: K, value: V)?) -> Dictionary<K, V> {
        return array.reduce([:]) {
            (var dict, e) in
            if let (key, value) = transformer(e)
            {
                dict[key] = value
            }
            return dict
        }
    }
    
}


//Distance String for Results
func getDistanceString(distance: Double) -> String {
    var str = ""
    if distance > 10000 {
        str = "More than 10000 km away"
    } else if distance >= 1 {
        str = "\(Int(distance)) km away"
    } else if distance < 1 {
        str = "\(Int(distance*1000)) m away"
    }
    return str
}


func downloadOffers(currencyFrom: String = "", currencyTo: String = "", amountFrom: NSNumber? = 0.0 , fromUser: PFUser? = nil, location: PFGeoPoint? = nil, completionHandler: ( (result: [PFObject]?) -> Void) ) {
    
    
    let offersQuery = PFQuery(className: "Offers")
    offersQuery.whereKey("status", notEqualTo: OFFER_STATUS_DELETED)
    offersQuery.whereKey("expiration", greaterThanOrEqualTo: NSDate())
    
    if fromUser != nil {
        offersQuery.whereKey("user", equalTo: fromUser!)
    } else {
        offersQuery.whereKey("user", notEqualTo: PFUser.currentUser()!)
    }
    offersQuery.includeKey("user")
    
    if (currencyFrom != "" && currencyTo != "" && amountFrom != nil) {
        offersQuery.whereKey("currencyFrom", equalTo: currencyFrom)
        offersQuery.whereKey("currencyTo", equalTo: currencyTo)
        offersQuery.whereKey("amountFrom", greaterThanOrEqualTo: amountFrom!)
        if location != nil { offersQuery.whereKey("location", nearGeoPoint: location!, withinKilometers: MAX_DISTANCE_MATCH) }
        offersQuery.orderByAscending("amountFrom")
        offersQuery.limit = 15
        
        offersQuery.findObjectsInBackgroundWithBlock {
            (objects: [PFObject]?, error: NSError?) -> Void in
            if error == nil {
                var result = objects!
                // -- Add a second query to load the lower half ---
                let offersQuery2 = PFQuery(className: "Offers")
                offersQuery2.whereKey("status", notEqualTo: OFFER_STATUS_DELETED)
                offersQuery2.whereKey("expiration", greaterThanOrEqualTo: NSDate())
                offersQuery2.whereKey("user", notEqualTo: PFUser.currentUser()!)
                offersQuery2.includeKey("user")
                offersQuery2.whereKey("currencyFrom", equalTo: currencyFrom)
                offersQuery2.whereKey("currencyTo", equalTo: currencyTo)
                offersQuery2.whereKey("amountFrom", lessThan: amountFrom!)
                offersQuery2.orderByDescending("amountFrom")
                offersQuery2.limit = 15
                return offersQuery2.findObjectsInBackgroundWithBlock {
                    (objects: [PFObject]?, error: NSError?) -> Void in
                    if error == nil {
                        result += objects as [PFObject]!
                        //------------- Sort Offers by difference to query ------------
                        result.sortInPlace({ (offer1, offer2) -> Bool in
                            let num1 : CGFloat = offer1["amountFrom"] as! CGFloat
                            let num2 : CGFloat = offer2["amountFrom"] as! CGFloat
                            let ref = amountFrom as! CGFloat
                            let diff1 = abs(num1 - ref)
                            let diff2 = abs(num2 - ref)
                            if ( diff1 < diff2 ) {
                                return true
                            } else { return false }
                        })
                        completionHandler(result: result)
                    } else {
                        dispatch_async(dispatch_get_main_queue(), {
                            ProgressHUD.showError("Network error")
                        })
                        print("Error: \(error) \(error!.userInfo)")
                    }
                }
            } else {
                // Log details of the failure
                dispatch_async(dispatch_get_main_queue(), {
                    ProgressHUD.showError("Network error")
                })
                print("Error: \(error) \(error!.userInfo)")
            }
        }
    } else {
        offersQuery.findObjectsInBackgroundWithBlock {
            (objects: [PFObject]?, error: NSError?) -> Void in
            if error == nil {
                completionHandler(result: objects)
            } else {
                // Log details of the failure
                dispatch_async(dispatch_get_main_queue(), {
                    ProgressHUD.showError("Network error")
                })
                print("Error: \(error) \(error!.userInfo)")
            }
        }
    }
}


// Bureau de Change search
func downloadBureauOffers(currencyFrom: String = "", currencyTo: String = "", amountFrom: NSNumber? = 0.0 , fromUser: PFUser? = nil, location: PFGeoPoint? = nil, completionHandler: ( (result: [PFObject]?) -> Void) ) {
    
    let offersQuery = PFQuery(className: "bureauOffer")
    offersQuery.whereKey("active", notEqualTo: false)
    offersQuery.whereKey("expiration", greaterThanOrEqualTo: NSDate())
    if location != nil {
        offersQuery.whereKey("location", nearGeoPoint: location!, withinKilometers: MAX_DISTANCE_MATCH)
    }
    offersQuery.limit = MAX_BUREAU_OFFERS
    offersQuery.findObjectsInBackgroundWithBlock {
        (objects: [PFObject]?, error: NSError?) -> Void in
        if error == nil {
            completionHandler(result: objects)
        } else {
            // Log details of the failure
            dispatch_async(dispatch_get_main_queue(), {
                ProgressHUD.showError("Network error")
            })
            print("Error: \(error) \(error!.userInfo)")
        }
    }
}




















//Checking with SII:
func checkSII(doc: PFObject, completionHandler: ((giros: [String]) -> Void)) {
    let id1 = doc[ID_NUMBER] as! String
    let index1 = id1.endIndex.advancedBy( -1 )
    let idNumber = id1.substringToIndex(index1) + "-" + id1.substringFromIndex(index1)
    
    let q1 = "https://siichile.herokuapp.com/consulta"
    let q2 = "?rut=" + idNumber
    let query = q1 + q2
    let url = NSURL(string: query)
    
    let session = NSURLSession.sharedSession()
    let task = session.dataTaskWithURL(url!, completionHandler: { (data, response, error) -> Void in
        guard data != nil else { print("error catching data"); return }
        if let urlContent = data {
            let jsonResult: AnyObject?
            do {
                jsonResult = try NSJSONSerialization.JSONObjectWithData(urlContent, options: NSJSONReadingOptions.MutableContainers)
            } catch { print("Error parsing JSON") ; return }
            guard jsonResult != nil else { print("No results returned. Could not save to memory."); return }
            
            guard let arr = jsonResult!.objectForKey("actividades") as? NSArray else {
                print("Error Unwrapping")
                return
            }
            var giros = [String]()
            print("** SII: Giros registrados por RUT:")
            if arr.count > 0 {
                for a in arr {
                    giros.append(a["giro"] as! String)
                    print(giros.last!)
                }
            } else { print("** No hay giros registrados") }
            completionHandler(giros: giros)
        }
    })
    task.resume()
}


/* ***************************************************** Check Validity of doc ************************************* */
//Check Validity
func checkValidId(doc: PFObject, completionHandler: ((isValid: Bool) -> Void)) {
    let id1 = doc[ID_NUMBER] as! String
    let index1 = id1.endIndex.advancedBy( -1 )
    let idNumber = id1.substringToIndex(index1) + "-" + id1.substringFromIndex(index1)
    
    let q1 = "http://www.simplesign.cl/users/check_chilean_id_with_rut"
    let q2 = "?rut=" + idNumber + "&serial=" + (doc[ID_SERIAL] as! String)
    let query = q1 + q2
    
    let url = NSURL(string: query)
    
    let session = NSURLSession.sharedSession()
    let task = session.dataTaskWithURL(url!, completionHandler: { (data, response, error) -> Void in
        guard data != nil else { print("error catching data"); return }
        if let urlContent = data {
            let jsonResult: AnyObject?
            do {
                jsonResult = try NSJSONSerialization.JSONObjectWithData(urlContent, options: NSJSONReadingOptions.MutableContainers)
            } catch { print("Error parsing JSON") ; return }
            guard jsonResult != nil else { print("No results returned. Could not save to memory."); return }
            
            if let valid = jsonResult!.objectForKey("valid_chilean_id") as? Int {
                let v: Bool
                if valid == 1 {
                    print("** REGISTRO CIVIL: The id is valid")
                    v =  true
                } else if valid == 0 {
                    print("** REGISTRO CIVIL: The id is not valid")
                    v = false
                } else {
                    print("not equivalent")
                    v = false
                }
                completionHandler(isValid: v)
            }
        }
    })
    task.resume()
}

/** ************************* BANCO DE CHILE API ************************************************ */

func BCH_API(service: String, request: NSDictionary, completionHandler: ((header: NSDictionary, body: NSDictionary?) -> Void)) {
    // prepare json data
    //        let body = [ "apiID":"27_SWP", "rut":"159485862"]
    let body = request
    //print(body)
    //let body = [ "apiID":"27_SWP", "rut":"159485862"]
    let jsonData = try? NSJSONSerialization.dataWithJSONObject(body, options: .PrettyPrinted)
    
    // create post request
    let str = "http://192.168.27.2:9001/BChHackatonAPI/webrest" + service + "?api_key=27_SWP"
    //let url = NSURL(string: "http://192.168.27.2:9001/BChHackatonAPI/webrest/cliente/dispositivos?api_key=27_SWP")!
    let url = NSURL(string: str)
    let request = NSMutableURLRequest(URL: url!)
    request.HTTPMethod = "POST"
    // insert json data to the request
    request.HTTPBody = jsonData
    request.addValue("application/json",forHTTPHeaderField: "Content-Type")
    let session = NSURLSession.sharedSession()
    let task = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
        guard data != nil else { print("error catching data"); return }
        if let urlContent = data {
            let jsonResult: AnyObject?
            do {
                jsonResult = try NSJSONSerialization.JSONObjectWithData(urlContent, options: NSJSONReadingOptions.MutableContainers)
            } catch { print("Error parsing JSON") ; return }
            guard jsonResult != nil else { print("No results returned. Could not save to memory."); return }
            guard let header = jsonResult!.objectForKey("header") as? NSDictionary else {
                print("Error Unwrapping"); return
            }
            // If there is data:
            let body = jsonResult!.objectForKey("body") as? NSDictionary ?? nil
            completionHandler(header: header, body: body)
        }
    })
    task.resume()
}


// PLACEHOLDER FOR KYC API


// For making Color Backgrounds
func getImageWithColor(color: UIColor, size: CGSize) -> UIImage {
    let rect = CGRectMake(0, 0, size.width, size.height)
    UIGraphicsBeginImageContextWithOptions(size, false, 0)
    color.setFill()
    UIRectFill(rect)
    let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return image
}

//  StringUtils.swift
//

import Foundation

extension String {
    
    // Returns true if the string has at least one character in common with matchCharacters.
    func containsCharactersIn(matchCharacters: String) -> Bool {
        let characterSet = NSCharacterSet(charactersInString: matchCharacters)
        return self.rangeOfCharacterFromSet(characterSet) != nil
    }
    // Returns true if the string contains only characters found in matchCharacters.
    func containsOnlyCharactersIn(matchCharacters: String) -> Bool {
        let disallowedCharacterSet = NSCharacterSet(charactersInString: matchCharacters).invertedSet
        return self.rangeOfCharacterFromSet(disallowedCharacterSet) == nil
    }
    // Returns true if the string has no characters in common with matchCharacters.
    func doesNotContainCharactersIn(matchCharacters: String) -> Bool {
        let characterSet = NSCharacterSet(charactersInString: matchCharacters)
        return self.rangeOfCharacterFromSet(characterSet) == nil
    }
    // Returns true if the string represents a proper numeric value.
    // This method uses the device's current locale setting to determine
    // which decimal separator it will accept.
    func isNumeric() -> Bool
    {
        let scanner = NSScanner(string: self)
        
        // A newly-created scanner has no locale by default.
        // We'll set our scanner's locale to the user's locale
        // so that it recognizes the decimal separator that
        // the user expects (for example, in North America,
        // "." is the decimal separator, while in many parts
        // of Europe, "," is used).
        scanner.locale = NSLocale.currentLocale()
        
        return scanner.scanDecimal(nil) && scanner.atEnd
    }
}


