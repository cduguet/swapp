//
//  WelcomeViewController.swift
//  SwApp
//
//  Created by Cristian Duguet on 7/19/15.
//  Copyright (c) 2015 CrowdTransfer. All rights reserved.
//

import UIKit
import Alamofire
import Parse
import ParseFacebookUtilsV4
import MediaPlayer

class WelcomeViewController: UIViewController {

    
    @IBOutlet var registerButton: UIButton!
    @IBOutlet var fbLoginButton: UIButton!
    var moviePlayer: MPMoviePlayerController!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // navigation bar style
        let bar:UINavigationBar! =  self.navigationController?.navigationBar
        bar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        bar.shadowImage = UIImage()
        bar.backgroundColor = UIColor(red: 0.0, green: 0.3, blue: 0.5, alpha: 0.0)
        bar.tintColor = UIColor.whiteColor()
        
        let videoURL = NSBundle.mainBundle().URLForResource("backgroundVideo", withExtension: "mp4")
        
        self.moviePlayer = MPMoviePlayerController(contentURL: videoURL)
        
        self.moviePlayer.controlStyle = MPMovieControlStyle.None
        self.moviePlayer.scalingMode = MPMovieScalingMode.AspectFill
        
        self.moviePlayer.view.frame = self.view.frame
        self.view .insertSubview(self.moviePlayer.view, atIndex: 0)
        
        self.moviePlayer.play()
        
        // Loop video.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "loopVideo", name: MPMoviePlayerPlaybackDidFinishNotification, object: self.moviePlayer)
        

        
    }
    
    func loopVideo() {
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.moviePlayer.play()
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func fbLogin(sender: AnyObject) {
        //ProgressHUD.show("Signing in...", interaction: false)
        dispatch_async(dispatch_get_main_queue()) { () -> Void in

        PFFacebookUtils.logInInBackgroundWithReadPermissions(PF_USER_PERMISSIONS) {
            (user: PFUser?, error: NSError?) -> Void in
            dispatch_async(dispatch_get_main_queue()) { () -> Void in
                ProgressHUD.show("Signing in...", interaction: false)
            }
            
            if error == nil {
                if let user = user {
                    if user.isNew {
                        self.requestFacebook(user)
                        print("User signed up and logged in through Facebook!")
                    } else {
                        self.requestFacebook(user)
                        //self.userLoggedIn(user)
                        print("User logged in through Facebook!")
                    }
                } else {
                    dispatch_async(dispatch_get_main_queue()) { () -> Void in
                        ProgressHUD.showError("Facebook sign in error")
                    }
                    print("Uh oh. The user cancelled the Facebook login.")
                }
            }
            else {
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    ProgressHUD.showError("Facebook Error")
                }
                print("Facebook threw an error: \(error?.userInfo)")
            }
        }
        }
    }
    
    func requestFacebook(user: PFUser) {
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, email, name, first_name, last_name, picture.type(large)"])
        graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
            if ((error) != nil)
            {
                // Process error
                print("Error: \(error)")
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    ProgressHUD.showError("Failed to fetch Facebook user data")
                }
                PFUser.logOut()
            }
            else
            {
                let userData = result as! NSDictionary
                self.processFacebook(user, userData: userData)
            }
        })
    }
    
    
    func processFacebook(user: PFUser, userData: NSDictionary) {
        let facebookUserId = userData.objectForKey("id") as? String
        //var link = userData.objectForKey("picture")?.objectForKey("data")?.objectForKey("url") as! String
        let link = "https://graph.facebook.com/\(facebookUserId!)/picture"
        //let url = NSURL(string: link)
        //var request = NSURLRequest(URL: url!)
        let params = ["height": "200", "width": "200", "type": "square"]
        Alamofire.request(.GET, link, parameters: params).response() {
            (request, response, data, error) in
            
            if error == nil {
                var image = UIImage(data: data! )!
                
                if image.size.width > 280 {
                    
                    image = Images.resizeImage(image, width: 280, height: 280)!
                    
                }
                let filePicture = PFFile(name: "picture.jpg", data: UIImageJPEGRepresentation(image, 0.6)!)
                filePicture.saveInBackgroundWithBlock({ (success: Bool, error: NSError?) -> Void in
                    if error != nil {
                        dispatch_async(dispatch_get_main_queue(), {
                            ProgressHUD.showError("Error saving photo")
                        })
                        
                    }
                })
                
                if image.size.width > 60 {
                    image = Images.resizeImage(image, width: 60, height: 60)!
                }
                let fileThumbnail = PFFile(name: "thumbnail.jpg", data: UIImageJPEGRepresentation(image, 0.6)!)
                fileThumbnail.saveInBackgroundWithBlock({ (success: Bool, error: NSError?) -> Void in
                    if error != nil {
                        dispatch_async(dispatch_get_main_queue(), {
                            ProgressHUD.showError("Error saving thumbnail")
                        })
                    }
                })
                
                if (filePicture as PFFile? != nil ) { user[PF_USER_PICTURE] = filePicture }
                else { print("No profile picture fetched") }
                if (fileThumbnail as PFFile? != nil) { user[PF_USER_THUMBNAIL] = fileThumbnail }
                else { print("No thumbnail fetched") }
                
                //print(user)
                
                // ------------------------- Get rest of user data --------------------------------------
                // CAnnot assign nil to PFObects, but NSNull
                user[PF_USER_FACEBOOKID] =  ( userData.objectForKey("id") as? String ?? user[PF_USER_FACEBOOKID]  ) ?? NSNull()
                user[PF_USER_FIRSTNAME] =  ( userData.objectForKey("first_name")  as? String ?? user[PF_USER_FIRSTNAME] ) ?? NSNull()
                user[PF_USER_LASTNAME]  =  ( userData.objectForKey("last_name")  as? String ?? user[PF_USER_LASTNAME] ) ?? NSNull()
                user[PF_USER_FULLNAME]  =  ( userData.objectForKey("name")  as? String ?? user[PF_USER_FULLNAME] ) ?? NSNull()
                user[PF_USER_EMAIL]     =  ( userData.objectForKey("email")  as? String ?? user[PF_USER_EMAIL] ) ?? NSNull()
                
                guard ( !user[PF_USER_FACEBOOKID].isEqual(NSNull()) &&
                    !user[PF_USER_FIRSTNAME].isEqual(NSNull()) &&
                    !user[PF_USER_LASTNAME].isEqual(NSNull()) &&
                    !user[PF_USER_FULLNAME].isEqual(NSNull()) &&
                    !user[PF_USER_EMAIL].isEqual(NSNull()) ) else {
                    self.performSegueWithIdentifier("confirmationSegue", sender: self)
                        return
                }
        
                user.saveInBackgroundWithBlock({ (succeeded: Bool, error: NSError?) -> Void in
                    if error == nil {
                        user[PF_USER_ACTIVATED] = true
                        self.userLoggedIn(user)
                    } else {
                        if let info = error?.userInfo {
                            dispatch_async(dispatch_get_main_queue(), {
                                ProgressHUD.showError("Login error: \(info) ")
                            })
                            print(info["error"] as! String)
                        }
                        PFUser.logOut()
                    }
                })
            } else {
                PFUser.logOut()
                if let info = error?.userInfo {
                    dispatch_async(dispatch_get_main_queue()) { () -> Void in
                        ProgressHUD.showError("Failed to fetch Facebook photo")
                    }
                    print(info["error"] as! String)
                }
            }
        }
    }
    
    // ******************************** Segue customization ************************************
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "confirmationSegue"{
            ProgressHUD.dismiss()
            let vc = segue.destinationViewController as! ConfirmationViewController
            vc.user = PFUser.currentUser()!
        }
    }

    // MARK: - Log in Function
    func userLoggedIn(user: PFUser) {
        PushNotication.parsePushUserAssign()
        dispatch_async(dispatch_get_main_queue(), {
            ProgressHUD.showSuccess("Welcome!")
        })
        //self.dismissViewControllerAnimated(true, completion: nil)
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let onboardingVC = storyboard.instantiateViewControllerWithIdentifier("OnboardingViewController") as! OnboardingViewController
//        self.parentViewController!.presentViewController(onboardingVC, animated: true, completion: nil)
        self.performSegueWithIdentifier("loginSegue", sender: self)
    }
}


