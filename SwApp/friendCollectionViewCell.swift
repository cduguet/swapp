//
//  friendCollectionViewCell.swift
//  SwApp
//
//  Created by Cristian Duguet on 8/11/15.
//  Copyright (c) 2015 CrowdTransfer. All rights reserved.
//


import UIKit
import Alamofire

class friendCollectionViewCell: UICollectionViewCell {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    var textLabel: UILabel = UILabel()
    var userImage: UIImageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        userImage = UIImageView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height))
        userImage.contentMode = UIViewContentMode.ScaleAspectFit
        contentView.addSubview(userImage)
        /*
        let textFrame = CGRect(x: 0, y: frame.size.height*2/3, width: frame.size.width, height: frame.size.height/3)
        textLabel = UILabel(frame: textFrame)
        textLabel.font = UIFont.systemFontOfSize(UIFont.smallSystemFontSize())
        textLabel.textAlignment = .Center
        contentView.addSubview(textLabel)
        */
    }
    
    func bindData(friendData: Dictionary<String,String>)
    {
        //FIXME: Check for null elements of class Offer
        userImage.layer.cornerRadius = userImage.frame.size.width / 2
        userImage.layer.masksToBounds = true
        
        let url = NSURL(string: friendData["pic"]!)
//        var request = NSURLRequest(URL: url!)
        Alamofire.request(.GET, url!).response() {
            (request, response, data, error) in
            if error == nil {
                self.userImage.image = UIImage(data: data!)!
            } else {
                print("error getting image")
            }
        }
    }
}
