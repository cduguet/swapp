//
//  myOffersTableViewController.swift
//  SwApp
//
//  Created by Cristian Duguet on 8/16/15.
//  Copyright (c) 2015 CrowdTransfer. All rights reserved.
//

import UIKit
import Parse

class myOffersTableViewController: UITableViewController {

    var swapper :PFUser? = PFUser()
    var offers = [PFObject]()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // --------------- Navigation Bar Style --------------
        // set bar element
        let bar:UINavigationBar! = self.navigationController?.navigationBar
        let img = getImageWithColor(SWAPP_BLUE, size: CGSizeMake(1, 1))
        bar.setBackgroundImage(img, forBarMetrics: UIBarMetrics.Default)
        bar.tintColor = UIColor.whiteColor()
        bar.shadowImage = nil
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: "refresh", forControlEvents: UIControlEvents.ValueChanged)
        self.tableView?.addSubview(self.refreshControl!)
        
        ProgressHUD.show("Loading...", interaction: false)
        updateFeed() { ProgressHUD.dismiss() }
    }
    
    func updateFeed( completionHandler: (() -> Void)) {
        
        let fromUser = swapper ?? PFUser.currentUser()
        downloadOffers("", currencyTo: "", amountFrom: 0.0, fromUser: fromUser) { (result: [PFObject]?) -> Void in
            self.offers.removeAll(keepCapacity: false)
            self.offers += result!
            self.tableView.reloadData()
            //self.updateEmptyView()
            completionHandler()
        }
    }

    func refresh() {
        updateFeed() { self.refreshControl!.endRefreshing() }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offers.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:OfferCell = self.tableView.dequeueReusableCellWithIdentifier("offerCell") as! OfferCell
        if (offers.count > 0) {
            cell.hasLabel.layer.borderWidth = 1
            cell.hasLabel.layer.borderColor = UIColor.grayColor().CGColor
            cell.wantsLabel.layer.borderWidth = 1
            cell.wantsLabel.layer.borderColor = UIColor.grayColor().CGColor
            
            cell.bindData(self.offers[indexPath.row])
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if swapper == nil { return true
        } else { return false }
    }

    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            
            offers[indexPath.row]["status"] = OFFER_STATUS_DELETED
            offers[indexPath.row].saveInBackgroundWithBlock({ (success, error) -> Void in
                if error == nil {
                    print("Succesfully deleted offer")
                    self.offers.removeAtIndex(indexPath.row)
                    tableView.beginUpdates()
                    // Delete the row from the data source
                    self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                    self.tableView.endUpdates()
                } else { print("error deleting") }
            })
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        let currentUser = PFUser.currentUser()
        if currentUser != nil {
            if currentUser![PF_USER_ACTIVATED] as? Bool == true {
            } else {
                Utilities.logout(self)
            }
        } else {
            Utilities.logout(self)
        }
    }
}
