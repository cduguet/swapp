// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.define("hello", function(request, response) {
  response.success("Hello world!");
});


Parse.Cloud.afterSave("Comments", function(request) {
	var query = new Parse.Query("Comments");
  	query.equalTo("aboutUser", request.object.get("aboutUser"));
  	query.find({
    	success: function(results) {
      		var sum = 0;
          var rating = 0;
          var raters = 0;
          if ( results.length > 0 ){ 
      		  for (var i = 0; i < results.length; ++i) {
        		  sum += results[i].get("opinion");
      		  }
          } else {
            console.log("results are zero \n");
          }
      		rating = sum / results.length;
          raters = results.length;
      		query = new Parse.Query("User");
			    query.get(request.object.get("aboutUser").id, {
			      success: function(user) {
              console.log(rating);
              Parse.Cloud.useMasterKey();
			    	  user.set("rating", rating);
              user.set("raters", raters);
              user.save(null,{
                success: function (object) { 
                  console.log(object);
                }, 
                error: function (object, error) { 
                  console.error(error);
                }
              });
			      },
			      error: function(error) {
			    	  console.error("Got an error " + error.code + " : " + error.message);
			      }
			    });
    	},
    	error: function() {
    	}
  	});
});