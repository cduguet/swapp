// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.define("hello", function(request, response) {
  response.success("Hello world!");
});


Parse.Cloud.afterSave("Comments", function(request) {
	var query = new Parse.Query("Comment");
  	query.equalTo("aboutUser", request.params.user);
  	query.find({
    	success: function(results) {
      		var sum = 0;
      		for (var i = 0; i < results.length; ++i) {
        		sum += results[i].get("stars");
      		}
      		var rating = sum / results.length;
      		query = new Parse.Query("User");
			query.get(request.object.get("aboutUser").id, {
			    success: function(user) {
			    	user.set("rating", rating);
			    	post.save();
			    },
			    error: function(error) {
			    	console.error("Got an error " + error.code + " : " + error.message);
			    }
			});
      		response.success(sum / results.length);
    	},
    	error: function() {
      		response.error("comment lookup failed");
    	}
  	});
});